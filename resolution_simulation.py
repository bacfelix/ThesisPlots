import ROOT
import pickle
import os
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter,  ETA, \
    AdaptiveETA,FileTransferSpeed, FormatLabel, Percentage, \
    ProgressBar, ReverseBar, RotatingMarker, \
    SimpleProgress, Timer

from math import sqrt
from root_style import *

style = root_style(batch=False)
style.set_style(1200,800,1./1.5)
style.set_main_dir('output/3-radiation/')
noises = [0,1,2,5,10]
hDigs = {}
pw = 50
entries = int(1e6)
stack = ROOT.THStack("stack",';residual / #mum;number of entries')
for i in noises:
    title = '%d%% Smearing'%i
    hDig = ROOT.TH1F('hDig_%d'%i,title+';residual / #mum;number of entries',1024,-2*pw,pw*2)
    hDig.SetLineColor(style.get_color(noises.index(i)))
    hDigs[i]  = hDig
    stack.Add(hDig)

pbar = ProgressBar(widgets=[Percentage(),
               ' ', Bar(),
               ' ', ETA(),
               ' ', AdaptiveETA()], maxval=entries).start()
for i in pbar(range(entries)):
    x_pred = ROOT.gRandom.Uniform(-.5,.5)
    for n in noises:
        dx = ROOT.gRandom.Gaus(0,n/100.)
        x_meas = round(x_pred+dx)
        res = x_pred-x_meas
        res *= pw
        # print x_pred,x_meas,dx,res
        hDigs[n].Fill(res)
pbar.finish()

c = style.get_canvas('c')
stack.Draw('nostack')
leg = style.make_legend(.7,.9,len(noises)*1.5)
for n in noises:
    h = hDigs[n]
    leg.AddEntry(h,h.GetTitle(),'l')
    leg.AddEntry(None,'#sigma = %5.2f #mum'%h.GetRMS(),'')
leg.Draw()
stack.GetXaxis().SetTitleOffset(1.4)
c.Update()

style.save_canvas(c,'digital_resolution_simulation')


