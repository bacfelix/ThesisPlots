__author__ = 'bachmair'
import math
import ROOT
from numpy import array
import json
import root_style
this_style = root_style.root_style()
style = root_style.root_style(batch=False)
style.set_main_dir('output/biasScans')
style.set_style(825,825,1/1)
canvas = style.get_canvas('test')
canvas.cd()
fdir = 'input/biasScans/'


settings_files = [
                  'poly_single.json',
                  # 'poly_ABCD.json',
                  #'biasScan_PolyD.json',
                  #'biasScan_PolyB2.json',
                  #'biasScan_3DpCVD.json',
                  # 'biasScan_PW205E.json',
                  # 'biasScan_PW205B.json'
                  # 'biasScan_S129.json'

]

bias_v = 300
for settings_file in settings_files:
    if settings_file != None:
        s_name = fdir+settings_file
        print s_name

        with open(s_name) as data_file:
            settings = json.load(data_file)
        print settings
        files = settings['files']
        output_file = settings['output_file']
        max_ccd = settings['max_ccd']
    else:
        settings = json.load(fdir+settings_file)
    xmax = settings.get("max_voltage", 1050.0)

    # files = {'PolyB2': 'biasScan_PolyB2.json'}
    # output_file='BiasScan_PolyB2'
    # max_ccd = 400
    min_ccd = -20


    xytitles = ';bias voltage / V;charge collection distance / #mum'
    def get_bias_scan_graph(name,data,marker_style=21,marker_color=ROOT.kBlack):
        V  = data['V']
        eV  = data['eV']
        CCD = data['CCD']
        eCCD = data['eCCD']
        offset = data['offset']
        CCD = map(lambda x: x-offset,CCD)
        if data.has_key('ignore_voltage'):
            print 'ignore: ',data['ignore_voltage']
            indices = map(lambda v: V.index(v), data['ignore_voltage'])
            V = [i for j, i in enumerate(V) if j not in indices]
            eV = [i for j, i in enumerate(eV) if j not in indices]
            CCD = [i for j, i in enumerate(CCD) if j not in indices]
            eCCD = [i for j, i in enumerate(eCCD) if j not in indices]

        g = ROOT.TGraphErrors(len(V),array(V,'f'),array(CCD,'f'),array(eV,'f'),array(eCCD,'f'))
        g.SetName(name)
        g.SetTitle(data['title']+xytitles)
        g.SetMarkerColor(marker_color)
        g.SetMarkerStyle(marker_style)
        g.SetMarkerSize(1.5)
        return g

    def get_thickness_graph(thickness):
        gThickness = ROOT.TCutG('gThickness',2)
        gThickness.SetPoint(0,-1e9,thickness)
        gThickness.SetPoint(1,+1e9,thickness)
        gThickness.SetLineStyle(2)
        gThickness.SetLineColor(ROOT.kBlue)
        gThickness.Draw('same')
        return gThickness



    graphs = {}
    colors = [ROOT.kBlack,ROOT.kRed,ROOT.kMagenta,ROOT.kBlue,ROOT.kGreen,ROOT.kOrange]
    marker_styles = [20,21,22,23,29,33,26]
    i = 0
    all_data = {}
    for dia,f in files.iteritems():
        fname = fdir+f
        print 'load',fname
        with open(fname) as data_file:
            data = json.load(data_file)
            all_data[dia]=data
            print data
            if dia in settings:
                data.update(settings[dia])
            runs = data['runs']
            print runs

            titles = data.get('titles',None)
            for run in runs:
                key = dia+'_'+run
                name = data['name']
                run_data = data[run]
                graphs[key]=get_bias_scan_graph(name,run_data,marker_style=marker_styles[i],marker_color=colors[i])
                if titles:
                    graphs[key].SetTitle(titles[runs.index(run)])
                i+=1

    mg = ROOT.TMultiGraph('mg',xytitles)
    # gs = [g25,g26,g27,g28]
    gs = sorted(graphs.items())
    lab = ROOT.TPaveLabel(.77,.87,.88,.94,'RD42','NDC NB')
    lab.SetShadowColor(0)
    lab.SetFillStyle(1001)
    lab.SetFillColor(0)
    lab.SetLineColorAlpha(0,0)
    lab.SetTextFont(62)

    all_ccds = []
    all_eccds = []
    for dia,g in gs:
        g.Draw('APE')
        lab.Draw()
        bias = [g.GetX()[i] for i in range(g.GetN())]
        ccds = [g.GetY()[i] for i in range(g.GetN())]
        eccds = [g.GetEY()[i] for i in range(g.GetN())]
        if bias_v in bias:
            all_ccds.append(ccds[bias.index(bias_v)])
            all_eccds.append(eccds[bias.index(bias_v)])
        maxy = max(ccds)
        # print maxy
        maxy*=1.3
        g.SetMaximum(maxy)
        g.SetMinimum(min_ccd)
        key = dia.split('_')[0]
        draw_thickness = True
        if all_data[key].has_key('draw_thickness'):
            draw_thickness = all_data[key]['draw_thickness']
        print 'draw_thickness', draw_thickness
        thickness = all_data[key]['thickness']
        if draw_thickness:
            gThickness=get_thickness_graph(thickness)
            gThickness.Draw('L')
        canvas.SetGridx()
        canvas.SetGridy()
        canvas.Update()
        style.save_canvas(canvas,'biasScan_'+dia)
        mg.Add(g,'PE')
    if len(all_ccds):
        print all_ccds,all_eccds
        ccd = reduce(lambda x, y: x + y, all_ccds) / len(all_ccds)
        eccd = math.sqrt(reduce(lambda x, y: x + y, map(lambda x: x ** 2, all_ccds)) / len(all_ccds)-ccd**2)
        print '\n',dia,'\nAvrg CCD at {bias:.0f} V: {ccd:.1f} +- {eccd:.1f} mum | {signal:.1f} '.format(
            bias = bias_v,
            ccd = ccd,
            eccd = eccd,
            signal = ccd*36

        )

    mg.Draw('A')
    mg.SetMaximum(max_ccd)
    mg.SetMinimum(min_ccd)
    mg.GetXaxis().SetRangeUser(-50,xmax)
    canvas.Update()
    mg.Draw('A')
    mg.GetXaxis().SetRangeUser(-50,xmax)

    mg.GetXaxis().SetLimits(-50, xmax)
    print gs
    # mg.GetXaxis().SetAxisColor(17)
    # mg.GetYaxis().SetAxisColor(17)
    # canvas.RedrawAxis()
    if len(files) == 1:
        if settings.get('draw_thickness',True):

            key = files.keys()[0]
            thickness = all_data[key]['thickness']
            draw_thickness = all_data[key]['draw_thickness'] if all_data[key].has_key('draw_thickness') else True
            if draw_thickness:
                gThickness=get_thickness_graph(thickness)
                gThickness.Draw('L')
                mg.Add(gThickness)
            print thickness

    # leg = style.make_legend(.18,.95,len(gs))
    x_ndc = settings.get('legend_x_ndc',.4)
    y_ndc = settings.get('legend_y_ndc',.4)
    leg = style.make_legend(x_ndc,y_ndc, len(gs))
    ggs = []
    for dia,g in gs:
        gg = g.Clone()
        gg.SetMarkerSize(3)
        gg.SetMarkerColor(g.GetMarkerColor())
        gg.SetMarkerStyle(g.GetMarkerStyle())
        l = leg.AddEntry(gg,g.GetTitle().split(';')[0],'P')
        l.SetTextColor(g.GetMarkerColor())
        ggs.append(gg)

    leg.Draw()
    canvas.SetGridx()
    canvas.SetGridy()
    canvas.Update()

    lab.Draw()
    canvas.Update()
    style.save_canvas(canvas,output_file)

