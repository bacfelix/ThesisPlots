import ROOT
import root_style
import copy
from array import array
import math
from copy import deepcopy
from time import sleep

style = root_style.root_style(batch=True)
style.set_main_dir('output/3-radiation/time_dependence/')
style.set_style(min_marg_right=.1*825)
c = style.get_canvas('c')
rep_error = 6.7
histos = {
}
runs = [160011,163031,171010,172080,191090,180030]
# runs = [180030]#160011,163031,171010,172080,191090,180030]
all_fits = {}
key = 'hLandauVsEventNo_2outOf10_pfx'
all_strings = {}
for run in runs:
    print run
    # f = ROOT.TFile.Open('input/3-radiation/histograms.transparent.%d.root' % run)
    f = ROOT.TFile.Open('input/3-radiation/c_hLandauVsEventNo_2outOf10_pfx.%d.root' % run)
    print f
    h = f.Get(key)
    print h
    if h == None:
        for k in f.GetListOfKeys():
            print
            if 'hLandauVsEventNo_2outOf10' in k.GetName():
                cc = f.Get(k.GetName())
                break
        h = cc.GetPrimitive('hLandauVsEventNo_2outOf10_pfx')
        h = deepcopy(h)
        h.GetYaxis().SetTitle('mean pulse height / ADC')
        h.UseCurrentStyle()
    print h
    c.cd()
    h.Draw('E')
    ROOT.gStyle.SetFitFormat('3.3g')
    ROOT.gStyle.SetOptFit(11)
    h.SetStats(False)
    h.SetMaximum(h.GetMaximum()*1.3)
    fit = h.GetFunction('fit_hLandauVsEventNo_2outOf10')
    fit.SetLineColor(ROOT.kBlue)
    p0 = fit.GetParameter(0)
    p1 = fit.GetParameter(1)
    e_p0 = fit.GetParError(0)
    e_p1 = fit.GetParError(1)
    h.GetXaxis().SetNdivisions(509)
    # h.GetXaxis().SetRangeUser(0,1e6)
    leg = style.make_legend(.15,.5,2,X2=.8)
    leg.SetNColumns(3)
    leg.AddEntry(None,'offset:','')
    leg.AddEntry(None, '{:6.1f} #pm {:4.1f}'.format(p0,e_p0),'')
    leg.AddEntry(None, 'ADC','')
    leg.AddEntry(None, 'slope:','')
    comb_error = root_style.combine_errors([e_p1*1e6, rep_error ])
    leg.AddEntry(None, '{:8.1f} #pm {:4.1f}'.format(p1*1e6, comb_error),'')
    leg.AddEntry(None, 'ADC/(1M events)', '')
    leg.Draw()
    style.save_canvas(c,'time_dependence.{runno}'.format(runno=run))
    histos[run] =  deepcopy(h)
    hh = deepcopy(h)
    nbins = hh.GetNbinsX()
    fitptr = h.Fit('pol1', 'QS+', '')
    print 'mean for ','000000', '{:+6.2f} +- {:6.2f}'.format(fitptr.Parameter(1) * 1e6, fitptr.ParError(1) * 1e6)
    ret_str = ''
    # min_dx = 400e3
    if nbins < 100:
        dxs = [900e3,450e3, 300e3]
    else:
        dxs = [197e4,980e3, 492e3]
    #
    # print run,nbins,dxs
    # for i in range(1):
    #     for min_dx in dxs:#range(int(100e3),int(1001e3),int(50e3)):
    #         bin_low = 1
    #         bin_up = 1
    #         data = []
    #         while bin_up < nbins:
    #             while True:
    #                 x_low = hh.GetXaxis().GetBinLowEdge(bin_low)
    #                 x_up = hh.GetXaxis().GetBinLowEdge(bin_up+1)
    #                 dx = x_up - x_low
    #                 if dx < min_dx and bin_up < nbins:
    #                     bin_up+=1
    #                 else:
    #                     break
    #             # print bin_low,bin_up,nbins,x_low, x_up
    #             if dx >=min_dx:
    #                 fitptr = h.Fit('pol1','QS+','',x_low,x_up)
    #                 data.append(fitptr.Parameter(1)*1e6)
    #                 dd = [fitptr.Parameter(1)*1e6,fitptr.ParError(1)*1e6]
    #                 print '%3d, %3d'%(bin_low,bin_up),
    #                 print '\t{:6.1f} +- {:6.1f}'.format(fitptr.Parameter(1)*1e6,fitptr.ParError(1)*1e6),
    #                 print '%6.1f'%fitptr.Parameter(0),'%6.1f'%fitptr.ParError(0),
    #                 print '%.1f'%fitptr.Chi2(),'%d'%fitptr.Ndf()
    #
    #             bin_low = bin_up
    #         if len(data):
    #             s = '['+','.join(map(lambda x:'%+5.1f'%x,data))+']'
    #             ret_str +=  '\t{i} {w} mean for {min_dx:6.0f} {x[0]:+6.2f} +- {x[1]:6.2f} {l} {d}\n'.format(
    #                 i=i,w=h.GetBinWidth(1),
    #                 min_dx = min_dx
    #                 ,x=root_style.calc_mean(data) if len(data) >1 else dd,l=len(data),d=s)
    #     h.Rebin()

    all_strings[run] = ( ret_str)
for run in runs:
    print run
    print all_strings[run]

if False:
        stack = ROOT.THStack('stack',';event no.;pulse height / ADC')
        for run in [160011,163031,171010,172080]:
            h = histos[run]
            scale = h.GetBinContent(h.GetXaxis().FindBin(100000))
            # h.Scale(1./scale)
            color = style.get_next_color()
            h.SetLineColor(color)
            h.SetMarkerColor(color)
            stack.Add(h)
        stack.Draw('nostack')
        style.save_canvas(c,'time_dependence.all'.format(runno=run))