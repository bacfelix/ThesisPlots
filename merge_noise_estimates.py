
import pickle
import os
from math import sqrt
import itertools

from root_style import *

force_load = False
f_all = './noise_estimate.all.p'
loaded = False
style = root_style(batch=False)
try:
    print 'Load all Pickle'
    peds = pickle.load( open(f_all,'rb'))
    loaded = True
    print 'loaded full pickle'
except Exception as e:
    print 'Couldn not load pickle du to'
    print e
    pass
if not loaded or force_load:
    peds = {}
    i = 0
    f_all = './noise_estimate.all.p'
    for f in os.listdir('.'):
        if not f.endswith('.p'):
            continue
        print type(f), f==f_all, "\"%s\""%f,"\"%s\""%f_all,
        if f.endswith(f_all) or f_all.endswith(f):
            print
            continue
        ped = pickle.load( open(f,'rb'))
        print 'add f',f
        i+=1
        for pl,ch in ped.iteritems():
            if not pl in peds:
                peds[pl] = []

            for i in range(len(ch)):
                if len(peds[pl]) <= i:
                    peds[pl].append([])
                peds[pl][i].append(ch[i])
    print 'merged data of %d runs'%i
    try:
        ensure_dir(f_all)
    except:
        pass
    pickle.dump(peds,open(f_all,'wb'))

print len(peds)
for i in peds:
    print i,len(peds[i]),
    if len(peds[i]) >0:
        print len(peds[i][0])
    else:
        print


def mean(l):
    return reduce(lambda x,y:x+y,l)/float(len(l))

def mean_and_sigma(l):
    mean_val = mean(l)
    l2 = map(lambda x:x**2,l)
    m2 = mean(l2)
    sigma = sqrt(m2 - mean_val**2)
    return mean_val,sigma

def analyse_list(l):
    return '%d Entries, %s, %s,%s'%(len(l),mean_and_sigma(l),min(l),max(l))

def get_list(peds,pl,no):
    if type(pl) == list:
        l2d = [get_list(peds,p,no) for p in pl]
        return list(itertools.chain.from_iterable(l2d))
    retval = []
    for ch in peds[pl]:
        for run in ch:
            retval.append(run[no])
    return retval




iterations_dia = get_list(peds,8,2)

print 'ITERATIONS:', min(iterations_dia),max(iterations_dia),mean_and_sigma(iterations_dia)
events_dia = get_list(peds,8,3)
pedestal_dia = get_list(peds,8,4)

sigma_dia = get_list(peds,8,5)

print 'EVENTS',min(events_dia),max(events_dia)
print 'pedestal',min(pedestal_dia),max(pedestal_dia),mean_and_sigma(pedestal_dia)
print 'sigma',min(sigma_dia),max(sigma_dia),mean_and_sigma(sigma_dia)
print 'ITERATIONS',analyse_list(iterations_dia)
print 'EVENTS',analyse_list(events_dia)
print 'pedestal',analyse_list(pedestal_dia)
print 'sigma',analyse_list(sigma_dia)


ll = [map(lambda x: x/5.,get_list(peds,pl,3)) for pl in [0,1,2,3,4,5,6,7]]
s_events,histos_events = create_stack(ll,'h_sil_events',500,0,100)
d_events = create_histo(map(lambda x: x/5.,get_list(peds,8,3)),'h_dia_events',500,0,100)
xtitle = 'rel. no. of Events Used for Pedestal Estimation / %'
ytitle = 'no. of entries'
s_events.Draw('goff')
s_events.GetXaxis().SetTitle(xtitle)
s_events.GetYaxis().SetTitle(ytitle)
s_events.SaveAs('initial_noise_estimate_events_silicon.root')
d_events.Draw('goff')
d_events.GetXaxis().SetTitle(xtitle)
d_events.GetYaxis().SetTitle(ytitle)
d_events.SaveAs('initial_noise_estimate_events_diamond.root')

ll = [get_list(peds,pl,2) for pl in [0,1,2,3,4,5,6,7]]
s_it,histos_it = create_stack(ll,'h_sil_iterations',10,0,10)
d_it = create_histo(get_list(peds,8,2),'h_dia_iterations',10,0,10)
xtitle = 'no. of iterations for pedestal calculation'
ytitle = 'no. of entries'
s_it.Draw('goff')
s_it.GetXaxis().SetTitle(xtitle)
s_it.GetYaxis().SetTitle(ytitle)
s_it.SaveAs('initial_noise_estimate_iterations_silicon.root')
d_it.Draw('goff')
d_it.GetXaxis().SetTitle(xtitle)
d_it.GetYaxis().SetTitle(ytitle)
d_it.SaveAs('initial_noise_estimate_iterations_diamond.root')

ll = [get_list(peds,pl,5) for pl in [0,1,2,3,4,5,6,7]]
s_sigma,histos_sigma = create_stack(ll,'h_sil_sigma',256,0,10)
d_sigma = create_histo(get_list(peds,8,5),'h_dia_sigma',1024,0,50)
xtitle = 'error on pedestal estimate'
ytitle = 'no. of entries'
s_sigma.Draw('goff')
s_sigma.GetXaxis().SetTitle(xtitle)
s_sigma.GetYaxis().SetTitle(ytitle)
s_sigma.SaveAs('initial_noise_estimate_sigma_silicon.root')
d_sigma.Draw('goff')
d_sigma.GetXaxis().SetTitle(xtitle)
d_sigma.GetYaxis().SetTitle(ytitle)
d_sigma.SaveAs('initial_noise_estimate_sigma_diamond.root')

ll = [get_list(peds,pl,4) for pl in [0,1,2,3,4,5,6,7]]
s_ped,histos_ped = create_stack(ll,'h_sil_pedestal',1024,0,256)
d_ped = create_histo(get_list(peds,8,4),'h_dia_pedestal',2*4096,0,4096)
xtitle = 'estimate for pedestal'
ytitle = 'no. of entries'
s_ped.Draw('goff')
s_ped.GetXaxis().SetTitle(xtitle)
s_ped.GetYaxis().SetTitle(ytitle)
s_ped.SaveAs('initial_noise_estimate_pedestal_silicon.root')
d_ped.Draw('goff')
d_ped.GetXaxis().SetTitle(xtitle)
d_ped.GetYaxis().SetTitle(ytitle)
d_ped.SaveAs('initial_noise_estimate_pedestal_diamond.root')