
import ROOT
import root_style
import copy
from copy import deepcopy
from collections import OrderedDict
style  = root_style.root_style(batch=False)
style.set_style(950,800,1)
style.set_main_dir('output/3-radiation/')
detectors = ['D0Y','D2Y']
runno = 17101
for det in []:#detectors:
    xmax = 150
    fname = 'input/3-radiation/hSignalLeftVsSignalRight{det}.{runno}.root'.format(
        det=det,
        runno=runno
    )
    f = ROOT.TFile.Open(fname)
    for k in f.GetListOfKeys():
        print k
        c = f.Get(k.GetName())
        break
    for p in c.GetListOfPrimitives():
        print p
        h = deepcopy(p)
    c = style.get_canvas('c')
    h.UseCurrentStyle()
    h.SetStats(False)
    h.GetXaxis().SetTitle('S_{Right} / ADC')
    h.GetXaxis().SetRangeUser(0,xmax)
    h.GetYaxis().SetTitle('S_{Left} / ADC')
    h.GetYaxis().SetRangeUser(0, xmax)
    h.GetZaxis().SetTitle('number of entries')
    h.Draw('colz')
    h.GetZaxis().SetTitleOffset(1.4)
    style.save_canvas(c,'hSignalLeftVsSignalRight{det}.{runno}'.format(
                                                                        det=det,
                                                                        runno=runno
                                                                    ))

style  = root_style.root_style(batch=False)
style.rel_marg_bot += .05
style.set_style(800,800,1)
fname = 'input/3-radiation/histograms.clustering.{runno}.root'.format(runno=runno)
typs = ['hEtaDistribution','hEtaIntegral']
typs = ['hEtaIntegral']
f = ROOT.TFile.Open(fname)
for t in []:
    for det in detectors:
        cname = 'c_{typ}_{det}'.format(det=det,typ=t)
        c = f.Get(cname)
        hname = '{typ}_{det}'.format(det=det,typ=t)
        h = deepcopy(c.GetPrimitive(hname))

        c = style.get_canvas('c')
        h.UseCurrentStyle()

        h.GetXaxis().SetTitle('#eta = #frac{S_{R}}{S_{L} + S_{R}}')
        if 'Integral' in t:
            h.GetYaxis().SetTitle('rel. #eta corrected position')
            h.GetXaxis().SetRangeUser(0,1)
            h.GetYaxis().SetRangeUser(0, 1)
            h.SetLineColor(ROOT.kBlue)
            pass

        h.GetXaxis().SetTitleOffset(1.5)
        h.Draw()
        style.save_canvas(c, '{typ}_{det}.{runno}'.format(
            det=det,
            runno=runno,
            typ = t
        ))
fname = 'input/3-radiation/histograms.alignment.{runno}0.root'.format(runno=runno)
t = "hSilicon_PostAlignment_Distribution_DeltaX"
f = ROOT.TFile.Open(fname)
detectors = ["Plane_1","Plane_2"]
for det in detectors:
    cname = 'c_{typ}_{det}'.format(det=det, typ=t)
    for k in f.GetListOfKeys():
        if t in k.GetName() and 'with_Chi' in k.GetName():
            if det in k.GetName():
                cname = k.GetName()
                break

    c = f.Get(cname)
    p = c.GetListOfPrimitives().At(0)

    h = deepcopy(p)

    c = style.get_canvas('c')
    h.UseCurrentStyle()
    print h.GetXaxis().GetBinWidth(2)
    for ff in h.GetListOfFunctions():
        print ff.GetName()
    fit = h.GetFunction("fitGausX")
    fit.SetLineColor(ROOT.kBlue)
    fit2 = deepcopy(fit)
    fit_range = max(abs(fit.GetMinimumX()),abs(fit.GetMaximumX()))
    print fit_range
    while h.GetXaxis().GetBinWidth(2) <  .5:
        h.Rebin()
    h.Fit(fit2,"Q","",-1*fit_range,fit_range)
    h.Fit(fit2, "Q", "", -1 * fit_range, fit_range)
    fit3 = deepcopy(fit2)
    # fit2.GetXaxis().SetXmin(-20)
    # fit2.GetXaxis().SetXmax(20)
    fit3.SetRange(-20,20)
    fit3.SetLineStyle(2)
    h.GetListOfFunctions().Add(fit3)
    h.GetXaxis().SetTitle('residual / #mum')
    h.GetYaxis().SetTitle('number of entries')
    if 'Integral' in t:
        h.GetYaxis().SetTitle('rel. #eta corrected position')
        h.GetXaxis().SetRangeUser(0, 1)
        h.GetYaxis().SetRangeUser(0, 1)
        h.SetLineColor(ROOT.kBlue)
        pass

    h.GetXaxis().SetTitleOffset(1.5)
    ROOT.gStyle.SetFitFormat('3.2f')

    h.Draw()
    stats = h.GetFunction("stats")
    stats.SetX1NDC(.6)
    stats.SetX2NDC(.99)
    stats.SetY1NDC(.8)
    stats.SetY2NDC(.99)
    stats.SetTextSize(.03)


    #fit2.Draw('same')
    style.save_canvas(c, '{typ}_{det}.{runno}'.format(
        det=det,
        runno=runno,
        typ=t
    ))

    # c2.Draw()
# histo = c2.GetPrimitive('hPulseHeightDistribution_D2Y')
# histo.RebinX(2)
# histos = []
# stack = ROOT.THStack('stack',';signal / ADC;number of entries')
# i_max = 3
# for i in range(1,i_max+1):
#     bin = histo.GetYaxis().FindBin(i)
#     if i< i_max:
#         h = histo.ProjectionX('_%d'%i,bin,bin)
#         title = 'cluster size = %d'%i
#     else:
#         h = histo.ProjectionX('_%d'%i,bin,100)
#         title = 'cluster size > %d'%(i-1)
#     h.SetTitle(title)
#     h.SetLineColor(style.get_color(i))
#     histos.append(h)
#     stack.Add(histos[-1])
# leg = style.make_legend(.48,.90,len(histos)*3+1,Y1=.5)
#
# leg.SetEntrySeparation(.8)
# hh = histo.ProjectionX('all',1,1)
# leg.AddEntry(None,'Mean_{all}: %5.1f #pm %4.1f'%(hh.GetMean(),hh.GetRMS()),'')
# print hh.GetXaxis().GetBinCenter(hh.GetMaximumBin())
# for h in histos:
#     leg.AddEntry(h,'','l')
#     mean = '\tMean: %5.1f #pm %4.1f'%(h.GetMean(),h.GetRMS())
#     mp = h.GetXaxis().GetBinCenter(h.GetMaximumBin())
#     leg.AddEntry(None,mean,'')
#     leg.AddEntry(None,'\tMP: %4.1f'%mp,'')
# c.cd()
# stack.Draw('nostack')
# stack.GetXaxis().SetRangeUser(0,400)
# stack.GetYaxis().SetRangeUser(.1,2e5)
# stack.Draw('nostack')
# leg.Draw()
# c.SetLogy()
# print leg.GetY1NDC()
#
# leg.SetEntrySeparation(.5)
# c.Modified()
# c.Update()
# leg2 = copy.deepcopy(leg)
# style.save_canvas(c,'ph_vs_clustersize_sil')
#
# stack2 = ROOT.THStack('stack2',';signal / ADC;rel. number of entries')
# histos2 = []
# for h in histos:
#     hh = copy.deepcopy(h.DrawNormalized('goff'))
#     hh.Rebin(4)
#     print h,hh
#     histos2.append(hh)
#     stack2.Add(hh)
# stack2.Draw('nostack')
# stack2.GetXaxis().SetRangeUser(0,300)
# leg2.Draw()