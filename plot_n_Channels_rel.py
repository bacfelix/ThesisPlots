
import ROOT
import root_style
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
c = style.get_canvas('c')


f = ROOT.TFile.Open('input/3-radiation/initial_noise_estimate_sigma_silicon.root')
stack = f.Get('h_sil_sigma')
stack.GetXaxis().SetRangeUser(0,3)
stack.Draw('nostack')
leg = style.make_legend(.6,.95,stack.GetNhists())
for h in stack.GetHists():
    leg.AddEntry(h,"",'l')
c.cd()
leg.Draw()
style.save_canvas(c,'sil_inital_noise')
c_logy = style.get_canvas('c_logy')
c_logy.SetLogy()
stack.SetMaximum(stack.GetMaximum()*10)
leg.SetNColumns(2)
c_logy.cd()
leg.SetColumnSeparation(.1)
leg.SetY1NDC(.6)
leg.ConvertNDCtoPad()
stack.Draw('nostack')
leg.Draw()
style.save_canvas(c_logy,'sil_inital_noise_logy')


f = ROOT.TFile.Open('input/3-radiation/initial_noise_estimate_iterations_silicon.root')
f2 = ROOT.TFile.Open('input/3-radiation/initial_noise_estimate_iterations_diamond.root')
c.cd()
stack2 = f.Get('h_sil_iterations')
stack2.Draw('nostack')
leg = style.make_legend(.6,.95,stack2.GetNhists())
for h in stack2.GetHists():
    leg.AddEntry(h,"",'l')
histo = f2.Get('h_dia_iterations')
histo.Scale(2)

histo.SetLineColor(ROOT.kBlue+3)
histo.Draw('same')
leg.AddEntry(histo,'Diamond (scaled)','L')
leg.Draw()
style.save_canvas(c,'inital_iterations')


#
# f = ROOT.TFile.Open('input/3-radiation/n_Channels_rel.root')
# h = f.Get('h_used_events')
# h.GetYaxis().SetTitle('Number of channels')
# h.GetXaxis().SetTitle('Fraction of Events for Pedestal Calculation / %')
# c = style.get_canvas('c')
# h.Draw()
# style.save_canvas(c,'n_Channels_rel')
#
#
# f = ROOT.TFile.Open('input/3-radiation/n_iterations.root')
# h = f.Get('h_n_iterations')
# h.GetYaxis().SetTitle('Number of Channels')
# h.GetXaxis().SetTitle('Number of Iterations')
# c = style.get_canvas('c')
# h.Draw()
# style.save_canvas(c,'h_n_iterations')
#
# f = ROOT.TFile.Open('input/3-radiation/dia_initial_noise.root')
# h = f.Get('c1')
# # h.GetYaxis().SetTitle('Number of Channels')
# # h.GetXaxis().SetTitle('Number of Iterations')
# h.Draw()
# style.save_canvas(g,'dia_inital_noise')
#
# f = ROOT.TFile.Open('input/3-radiation/initial_noise_estimate_sigma_silicon.root')
# h = f.Get('h_sil_sigma')
# # h.GetYaxis().SetTitle('Number of Channels')
# # h.GetXaxis().SetTitle('Number of Iterations')
# h.Draw()
# style.save_canvas(g,'dia_inital_noise')