from copy import deepcopy
import ROOT
import root_style
import copy
from copy import deepcopy
from collections import OrderedDict

style  = root_style.root_style(batch=False)
style.set_style(825,825,1)
style.set_main_dir('output/3-radiation/')

runno = 150042
keys = OrderedDict([
    # ('pulseheightOne','hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    # ('pulseheightTwo','hDiaTranspAnaPulseHeightOf2HighestIn10Strips'),
    # ('efficiencyTwo', 'hEfficiency_hDiaTranspAnaPulseHeightOf2HighestIn10Strips'),
    # ('efficiency','hEfficiency_hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    # ('efficiency2', 'hEfficiency_hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    ('resolution','hDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred')
    ])
fdir = 'input/3-radiation/'
fname = 'histograms.transparent.{runno}.root'.format(runno=runno)
fpath = fdir + fname
f = ROOT.TFile.Open(fpath)
factor = 100./7.6*1.02
noise = 79.2
histos = OrderedDict()
for key, item in keys.items():
    if key == 'resolution':
        fname2 = 'cDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred_FixedDoubleGausFit.{runno}.root'.format(runno=runno)
        fpath2 = fdir + fname2
        f2 = ROOT.TFile.Open(fpath2)
        c2 = f2.Get('cDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred_FixedDoubleGausFit')
        h = c2.GetPrimitive('hDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred_FixedDoubleGausFit')
    else:
        h = f.Get(item)
    print h
    if h == None:
        print 'cannot find ',key
        for k in f.GetListOfKeys():
            if item in k.GetName():
                print ' *',k
        continue
    h.UseCurrentStyle()
    leg = None
    c = style.get_canvas('c')
    if key == 'resolution':
        hh = deepcopy(h)
        hh.UseCurrentStyle()
        hh.GetXaxis().SetTitle('residual / #mum')
        hh.GetYaxis().SetTitle('number of entries')
        hh.Draw()
        stats = hh.GetFunction('stats')
        stats.SetTextSize(.03)
    elif 'efficiency' not in key:
        hh = style.get_x_calibrated_histo(h,factor,xTitle='signal / #it{e}')
        hh.GetXaxis().SetRangeUser(0, 8000)
        hmax = hh.GetMaximumBin()
        xmin = hh.GetXaxis().GetBinLowEdge(hmax - 4)
        xmax = hh.GetXaxis().GetBinLowEdge(hmax + 6)
        print hmax, xmin, xmax
        fit = ROOT.TF1('fit', 'gaus', xmin, xmax)
        hh.Fit(fit,'Q0','',xmin,xmax)
        hh.SetStats(False)
        print key
        print '  MP:   {:.0f} +- {:.1f}'.format(fit.GetParameter(1),fit.GetParError(1))
        print '  Mean: {:.0f} +- {:.1f}'.format(hh.GetMean(),hh.GetMeanError())
        leg = style.make_legend(.6,.9,2)
        leg.SetMargin(0.)
        leg.AddEntry(None,'MP:','')
        leg.AddEntry(None,'{:.0f} #it{{e}}'.format(round(fit.GetParameter(1),-1),fit.GetParError(1)),'')
        leg.AddEntry(None, 'Mean: ','')
        leg.AddEntry(None, '{:.0f} #it{{e}}'.format(round(hh.GetMean(),-1),hh.GetMeanError()), '')
        leg.SetNColumns(2)
    else:
        if key == 'efficiency2':
            hh = style.get_x_calibrated_histo(h, factor/noise,xTitle='threshold in SNR')
            hh.GetXaxis().SetRangeUser(0, 20)
        else:
            hh = style.get_x_calibrated_histo(h, factor, xTitle='threshold / #it{e}')
            if key == 'efficiency':
                hh.GetXaxis().SetRangeUser(0, 1000)
                for i in range(1,10):
                    print hh.GetBinCenter(i),hh.GetBinContent(i)
                fit = ROOT.TF1('fit','pol6',0,1000)
                hh.Fit(fit,'Q0')
                hh.SetStats(False)
                print 500,fit(500)
        hh.GetXaxis().SetNdivisions(507)
        c.SetGridx()
    histos[key] = deepcopy(hh)
    hh.Draw()
    if leg:
        leg.Draw()
    hh.GetFunction("stats")

    c.Update()
    stats = hh.GetFunction("stats")
    print stats
    if  key == 'resolution' and stats != None:
        ROOT.gStyle.SetFitFormat('4.2g')
        hh.Draw()
        print stats,stats.GetX1NDC(),stats.GetX2NDC(),stats.GetY1NDC(),stats.GetY2NDC()
        stats.SetX1NDC(0.64)
        stats.SetX2NDC(0.99)
        stats.SetY1NDC(0.61)
        stats.SetY2NDC(0.99)


    style.save_canvas(c,'{key}.{runno}'.format(key=key,runno=runno))
    raw_input()
if False:
    stack = ROOT.THStack('stack',';signal / #it{e}; rel. number of entries')
    leg = style.make_legend(.6,.9,2)
    for k,h in histos.items():
        if 'pulse' in k:
            h.SetLineColor(style.get_next_color())
            h.Scale(1./h.GetBinContent(h.GetMaximumBin()))
            stack.Add(h)
            if k.endswith('One'):
                leg.AddEntry(h,'one channel','l')
            elif k.endswith('Two'):
                leg.AddEntry(h, 'two channels', 'l')
    c.SetGridx(False)
    stack.Draw('nostack')
    stack.GetXaxis().SetRangeUser(0, 8000)
    leg.Draw()
    style.save_canvas(c,'pulseheightBoth.{runno}'.format(key=key,runno=runno))
