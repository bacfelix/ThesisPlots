__author__ = 'bachmair'
import ROOT
from numpy import array
import json
import root_style
this_style = root_style.root_style()
style = root_style.root_style(batch=False)
style.set_main_dir('output/1-intro')
style.set_style(800,800,1)#1/1.2)
canvas = style.get_canvas('test')
canvas.cd()

f = 'diamond_area.json'
output_file='diamond_area'


colors = [ROOT.kBlack,ROOT.kRed,ROOT.kMagenta,ROOT.kBlue]
marker_styles = [20,21,22,23]
xytitles = ';year ;active area / cm^{2}'
def get_bias_scan_graph(name,data,marker_style=21,marker_color=ROOT.kBlack):
    x = [round(float(i)) for i in data['x']]
    x = array(x,'f')
    y = [float(i) for i in data['y']]
    y = array(y,'f')
    print x, min(x), max(x)
    print y
    print len(x)
    g = ROOT.TGraph(len(x))
    for i  in range(len(x)):
        g.SetPoint(i,x[i],y[i])
        print i,x[i],y[i]
    g.SetName(name)
    g.SetTitle(data['title']+xytitles)
    g.SetMarkerColor(marker_color)
    g.SetMarkerStyle(marker_style)
    g.SetMarkerSize(1.5)
    return g


fname = 'input/1-intro/' + f
print 'load', fname
with open(fname) as data_file:
    data = json.load(data_file)
    graph = get_bias_scan_graph('diamond_area', data, marker_style=25,
                                                      marker_color=colors[0])
frame = canvas.DrawFrame(2000,1,2020,1000,xytitles)
graph.Draw('P')
# graph.SetMinimum(1)
# graph.SetMaximum(1000)
# graph.GetXaxis().SetRangeUser(2000,2018)
frame.GetXaxis().SetNdivisions(507)
frame.GetYaxis().SetTitleOffset(1.4)
l  = ROOT.TLatex()
# l.SetTextSize(0.025);
# l.SetTextAngle(30.);
for i in range(len(data['titles'])):
    title = data['titles'][i]
    if title != 'CMS':
        x = float(data['x'][i])+.4
        y = float(data['y'][i])*.8
    else:
        x = float(data['x'][i])-.4
        y = float(data['y'][i])*1.2
    l.DrawLatex(x,y,title)
    print x,y, title


canvas.SetLogy()
canvas.Update()
style.save_canvas(canvas,output_file)

