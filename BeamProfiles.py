
import ROOT
import root_style
import array
import copy
import csv
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
c = style.get_canvas('c')
input_dir = 'input/3-radiation/'
input_type = 'csv'
for input_file in ['H6a-BeamProfile-05May2016-Y','H6a-BeamProfile-05May2016-X']:
    file_name = input_dir+input_file+'.'+input_type
    with open(file_name, 'rb') as f:
        reader = csv.reader(f)
        data = list(reader)
    # print data
    data = [(float(i[0]),float(i[1])) for i in data]
    data = sorted(data)
    x = [float(i[0]) for i in data]
    y = [float(i[1]) for i in data]
    x = array.array('d',x)
    y = array.array('d',y)
    # print x,y

    gr = ROOT.TGraph(len(x),x,y)
    gr.SetTitle(';position / mm; particle counts')
    gr.Draw('APL')
    gr.GetYaxis().SetTitleOffset(1.6)
    gr.GetXaxis().SetRangeUser(-30,30)
    style.save_canvas(c,input_file)
    fr = gr.Fit('gaus','SQ')
    print fr.Parameter(1),fr.Parameter(2)
    h = gr.PaintGrapHist(len(x),x,y,'hist')
    c.Update()
    # h.Draw()
    h = ROOT.TH1F('h',';position / mm; particle counts',120,-30,30)
    for d in data:
        h.SetBinContent(h.FindBin(d[0]),d[1])
    hm = h.GetBinContent(h.GetMaximumBin())/2.
    hm_low = h.GetBinCenter(h.FindFirstBinAbove(hm))
    hm_up = h.GetBinCenter(h.FindLastBinAbove(hm))
    fwhm = hm_up-hm_low
    print fwhm

    h.Draw()

    raw_input()