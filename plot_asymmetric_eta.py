
import ROOT
import root_style
import copy
import time
from copy import deepcopy
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
style.rel_marg_bot += .05
style.set_style(800,800,1)
c = style.get_canvas('c')
fdir = 'input/3-radiation/'
fname = 'histograms.transparent.{runno}.root'
# runs = [19109,17101,
runs = [16302,163021]
hnames = {
    'eta_distribution':'hDiaTranspAnaEtaCMNcorrected2HighestIn10Strips',
    'eta_resolution':'hDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred',
    'eta_pulseheight':'hDiaTranspAnaPulseHeightOf2HighestIn10Strips'
}
histos = {}
for key in hnames:
    histos[key] = {}
def create_mirrored_distribution(h):
    h2 = h.Clone()
    nbins = h.GetNbinsX()+1
    for bin in range(1,nbins):
        content = h.GetBinContent(nbins-bin)
        h2.SetBinContent(bin,content)
    h2.SetLineColor(ROOT.kBlue)
    h2.SetStats(False)
    h.GetListOfFunctions().Clear()
    return h2
for key,hname in hnames.items():
    for run in runs:
        fpath = fdir+fname.format(runno=run)
        f = ROOT.TFile.Open(fpath)
        print fpath
        for k in f.GetListOfKeys():
            if hname == k.GetName():
                h = f.Get(k.GetName())
                break
        if h == None:
            for k in f.GetListOfKeys():
                if "c_"+hname in k.GetName():
                    cc = f.Get(k.GetName())
                    print cc,k
                    break
            hh = cc.GetPrimitive(hname).Clone()
            h = deepcopy(hh)
        c.cd()
        h.UseCurrentStyle()
        h.GetListOfFunctions().Clear()
        h.GetXaxis().SetTitleOffset(1.5)
        if key == 'eta_distribution':
            h.GetXaxis().SetTitle('#eta = #frac{S_{R}}{S_{L} + S_{R}}')

        elif key == 'eta_resolution':
            h.GetXaxis().SetTitle('residual / #mum')
            h.GetListOfFunctions().Clear()
            while h.GetXaxis().GetBinWidth(2) <.3:
                h.Rebin()
            h.GetXaxis().SetRangeUser(-20, 20)
        print h.GetXaxis().GetBinWidth(2)
        h.GetYaxis().SetTitle('number of entries')
        h.Draw()
        if key == 'eta_distribution':
            h2 = create_mirrored_distribution(h)
            h2.Draw('same')
        c.Update()
        style.save_canvas(c,'{key}.{run}'.format(run=run,key=key))
        histos[key][run] = deepcopy(h)

stacks = []
for k in ['eta_resolution','eta_pulseheight']:
    if k == 'eta_resolution':
        xytitle = ';residual / #mum;number of entries'
        leg_pos = .2
    else:
        xytitle = ';pulse height / ADC;number of entries'
        leg_pos  = .6
    stack = ROOT.THStack('stack', xytitle)
    i = 1
    leg = style.make_legend(leg_pos, .9, 2)
    for runno,h in sorted(histos[k].items()):
        print runno, (runno %16302)
        if runno == 16302 or runno == 163021:
            print runno
            if k == 'eta_resolution':
                h.Scale(1./h.GetBinContent(h.GetMaximumBin()))
            h.SetLineColor(i)
            i+=1
            if runno == 16302:
                h.SetTitle('original')
            else:
                h.SetTitle('corrected')
            leg.AddEntry(h,h.GetTitle(),'l')
            stack.Add(h)
            print runno,h.GetMean()
    stack.Draw('nostack')
    if k == 'eta_resolution':
        stack.GetXaxis().SetRangeUser(-20,20)
    leg.Draw()
    style.save_canvas(c,'{key}.combined'.format(key=k))
    stacks.append(stack)


fdir = 'input/3-radiation/'
fname = 'correction.root'

f = ROOT.TFile.Open(fdir+fname  )
h = f.Get('h')
h.UseCurrentStyle()
h.GetXaxis().SetTitle('#frac{PH_{corrected}}{PH_{original}}')
h.GetYaxis().SetTitle('number of entries')
h.Draw()
h.GetXaxis().SetTitleOffset(1.5)
style.save_canvas(c,'pulseheight_correction')


