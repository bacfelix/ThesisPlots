
import ROOT
import root_style
import copy
from copy import deepcopy
from collections import OrderedDict
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')

fname = 'input/3-radiation/residual_digital_diamond.root'
f = ROOT.TFile.Open(fname)
cname ='c_hDiamond_PostAlignment_Distribution_DeltaX_Plane_4_with_0_1_2_and_3'
c2 = f.Get(cname)
hname = 'hDiamond_PostAlignment_Distribution_DeltaX_Plane_4_with_0_1_2_and_3'
h = deepcopy(c2.GetPrimitive(hname))
c = style.get_canvas('c')
h.Draw()
ROOT.gStyle.SetOptFit(111)
ROOT.gStyle.SetOptStat(1100)
ROOT.gStyle.SetFitFormat('5.2f')
ROOT.gStyle.SetStatFormat('5.2f')
h.UseCurrentStyle()
h.SetStats(True)

# h.Draw('same')
for f in h.GetListOfFunctions():
    print f.GetName()
    if 'PrevFitTMP' in f.GetName():
        ff = f
        ff.SetLineColor(ROOT.kBlue)
        ff.SetParName(0,'Constant')
        ff.SetParName(1, 'Sigma')
        ff.SetParName(2, 'Position')
        print 'blue'
    if 'stats' in f.GetName():
        stats = f
        stats.SetOptFit(111)
        stats.SetOptStat(1100)
        stats.SetFitFormat('5.2f')
        stats.SetStatFormat('5.2f')
        c.Modified()
        c.Update()
ff.SetLineColor(ROOT.kBlue)



style.save_canvas(c,'residual_digital_diamond')
# c2.Draw()
# histo = c2.GetPrimitive('hPulseHeightDistribution_D2Y')
# histo.RebinX(2)
# histos = []
# stack = ROOT.THStack('stack',';signal / ADC;number of entries')
# i_max = 3
# for i in range(1,i_max+1):
#     bin = histo.GetYaxis().FindBin(i)
#     if i< i_max:
#         h = histo.ProjectionX('_%d'%i,bin,bin)
#         title = 'cluster size = %d'%i
#     else:
#         h = histo.ProjectionX('_%d'%i,bin,100)
#         title = 'cluster size > %d'%(i-1)
#     h.SetTitle(title)
#     h.SetLineColor(style.get_color(i))
#     histos.append(h)
#     stack.Add(histos[-1])
# leg = style.make_legend(.48,.90,len(histos)*3+1,Y1=.5)
#
# leg.SetEntrySeparation(.8)
# hh = histo.ProjectionX('all',1,1)
# leg.AddEntry(None,'Mean_{all}: %5.1f #pm %4.1f'%(hh.GetMean(),hh.GetRMS()),'')
# print hh.GetXaxis().GetBinCenter(hh.GetMaximumBin())
# for h in histos:
#     leg.AddEntry(h,'','l')
#     mean = '\tMean: %5.1f #pm %4.1f'%(h.GetMean(),h.GetRMS())
#     mp = h.GetXaxis().GetBinCenter(h.GetMaximumBin())
#     leg.AddEntry(None,mean,'')
#     leg.AddEntry(None,'\tMP: %4.1f'%mp,'')
# c.cd()
# stack.Draw('nostack')
# stack.GetXaxis().SetRangeUser(0,400)
# stack.GetYaxis().SetRangeUser(.1,2e5)
# stack.Draw('nostack')
# leg.Draw()
# c.SetLogy()
# print leg.GetY1NDC()
#
# leg.SetEntrySeparation(.5)
# c.Modified()
# c.Update()
# leg2 = copy.deepcopy(leg)
# style.save_canvas(c,'ph_vs_clustersize_sil')
#
# stack2 = ROOT.THStack('stack2',';signal / ADC;rel. number of entries')
# histos2 = []
# for h in histos:
#     hh = copy.deepcopy(h.DrawNormalized('goff'))
#     hh.Rebin(4)
#     print h,hh
#     histos2.append(hh)
#     stack2.Add(hh)
# stack2.Draw('nostack')
# stack2.GetXaxis().SetRangeUser(0,300)
# leg2.Draw()