from copy import deepcopy
import ROOT
import root_style
import copy
from copy import deepcopy
from collections import OrderedDict

style  = root_style.root_style(batch=False)
style.rel_marg_top=.05
style.set_style(825,825,1,)
style.set_main_dir('output/1-intro/')

runnos = OrderedDict([(17107,'scCVD'),
                      (19106,'pCVD')])
keys = OrderedDict([
    # ('pulseheightOne','hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    # ('pulseheightTwo','hDiaTranspAnaPulseHeightOf2HighestIn10Strips'),
    # ('efficiencyTwo', 'hEfficiency_hDiaTranspAnaPulseHeightOf2HighestIn10Strips'),
    # ('efficiency','hEfficiency_hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    # ('efficiency2', 'hEfficiency_hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    ('signal','ph_strip_vs_3d_electrons')
    ])
histos = OrderedDict()
fdir = 'input/1-intro/'
stack = ROOT.THStack('stack', ';signal / #it{e}; rel. number of entries')

# ROOT.gStyle.SetFitFormat('.1f')
ROOT.gStyle.SetStatFormat('.1f')
ROOT.gStyle.SetOptStat(201)
ROOT.gStyle.SetOptFit(0)
parameters={}
for runno,typ  in runnos.items():
    fname = 'ph_strip_vs_3d_electrons.{runno}.root'.format(runno=runno)
    fpath = fdir + fname
    f = ROOT.TFile.Open(fpath)
    c = f.Get('c3')
    hs = c.GetPrimitive('hs3')
    h = hs.GetHists().FindObject('hLandauStrip_trans_electrons').Clone()

    if typ =='pCVD':
        h = style.get_x_calibrated_histo(h,1.8)
    if typ == 'scCVD':
        pass
        h = deepcopy(h)
        #style.get_x_calibrated_histo(h, 500/440.)
    h.SetName(typ)
    h = deepcopy(h)
    histos[runno] = h
    print '%.1f'%(h.GetMean()/36.)
    h.UseCurrentStyle()
    color = style.get_next_color()
    h.SetLineColor(color)
    stack.Add(h)
    fwhm,xlow,xup = root_style.get_fwhm(h)
    fitres = h.Fit('gaus','QS','',xlow,xup-.3*fwhm)
    print '{:.1f} +- {:.1f}'.format(fitres.Parameter(1),fitres.ParError(1))
    parameters[runno] = {'MP':(fitres.Parameter(1),fitres.ParError(1)),
                         'FWHM':fwhm}
    h.Fit('landau','Q')
    f = h.GetListOfFunctions().FindObject('landau')
    f.SetLineColor(color)
    f.SetLineStyle(2)
    f.SetNpx(1000)
c2 = style.get_canvas()
stats = {}
legends = {}
for runno,h in histos.items():
    hh = deepcopy(h)
    h.SetStats(True)
    h.Draw()
    c2.Update()
    stat = h.GetListOfFunctions().FindObject('stats')
    stat.SetFitFormat('.1f')
    stats[runno] = deepcopy(stat)
    mp = parameters[runno]['MP']
    fwhm = parameters[runno]['FWHM']
    mp = (round(mp[0],-2),round(mp[1],-2))
    mean = (round(h.GetMean(), -2), round(h.GetMeanError(), -2))
    fwhm = round(fwhm, -2)
    ratio = round(fwhm/mp[0],2)
    if runnos[runno] == 'scCVD':
        leg = style.make_legend(.55,.93,4)
        t = 466
    else:
        leg = style.make_legend(.55, .6, 4)
        t = 525
    leg.SetNColumns(2)
    leg.AddEntry(h,runnos[runno]+', ','l')
    leg.AddEntry(None, ' t: {:3.0f} #mum'.format(t), '')
    leg.AddEntry(None,'Mean: ','')
    leg.AddEntry(None, '{x[0]:5.0f} #pm {x[1]:.0f} e'.format(x=mean),'')
    leg.AddEntry(None, 'MP: ', '')
    leg.AddEntry(None, '{x[0]:5.0f} #pm {x[1]:.0f} e'.format(x=mp),'')
    leg.AddEntry(None, 'FWHM: ', '')
    leg.AddEntry(None, '{x:5.0f} e'.format(x=fwhm),'')
    leg.AddEntry(None, 'Ratio: ', '')
    leg.AddEntry(None, '{x:5.2f} '.format(x=ratio),'')
    legends[runno]=leg
    stats[runno].AddText('MP  =  {x[0]:.0f} #pm {x[1]:.0f}'.format(x=mp))
    stats[runno].AddText('FWHM  =  {x:.0f}'.format(x=fwhm))
    stats[runno].AddText('Ratio  =  {x:.2f}'.format(x=ratio))

for h in stack.GetHists():
    h.SetStats(False)
c2.SetGridx(False)
stack.Draw('nostack')
stack.GetXaxis().SetRangeUser(0, 40000)
stack.GetXaxis().SetNdivisions(505)
c2.Update()
for run, s in legends.items():
    s.Draw()

style.save_canvas(c2,'signal_pCVD_scCVD')

