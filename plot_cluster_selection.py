from numpy import array as ar
from collections import OrderedDict
import ROOT
import root_style
import copy
import array
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
c = style.get_canvas('c')
def bitcount(n):
    count = 0
    while n > 0:
        count = count + 1
        n = n & (n-1)
    return count

def create_ndetector_hit_histo(histo):
    values = {}
    for i in range(0,histo.GetNbinsX()+0):
        n = bitcount(i)
        if not n in values:
            values[n] = 0
        values[n] += histo.GetBinContent(i+1)
    name = histo.GetName()+"_detHit"
    xmin = min(values.keys())
    xmax = max(values.keys())
    bins = xmax-xmin+1
    retHist = ROOT.TH1F(name,name,bins,xmin-.5,xmax+.5)
    # return values,retHist
    for n in values:
        ntimes = int(values[n])
        if ntimes == 0:
            continue
        retHist.FillN(ntimes,array.array('d',[n]*ntimes),array.array('d',[1]*ntimes),1)
    return retHist

f = ROOT.TFile.Open('input/3-radiation/histograms.19106.selection.root')
h1 = f.Get('hSiliconClusterBitMask')
h2 = f.Get('hSiliconOneAndOnlyOneClusterBitMask')
h3 = f.Get('hSiliconValidClusterHitBitMask')
h4 = f.Get('hSiliconOneAndOnlyOneValidClusterHitBitMask')

h11 = create_ndetector_hit_histo(h1)
h11.SetTitle('at least 1 cluster')
h21 = create_ndetector_hit_histo(h2)
h21.SetTitle('exactly 1 cluster')
h31 = create_ndetector_hit_histo(h3)
h31.SetTitle('not masked/not saturated cluster')
h41 = create_ndetector_hit_histo(h4)
h41.SetTitle('exaclty one valid cluster')
h11.SetLineColor(ROOT.kBlack)
h21.SetLineColor(ROOT.kRed)
h31.SetLineColor(ROOT.kGreen)
h41.SetLineColor(ROOT.kBlue)
h13 = copy.deepcopy(h11)
h23 = copy.deepcopy(h21)
h33 = copy.deepcopy(h31)
h43 = copy.deepcopy(h41)

h12 = copy.deepcopy(h11.DrawNormalized())
h22 = copy.deepcopy(h21.DrawNormalized())
h32 = copy.deepcopy(h31.DrawNormalized())
h42 = copy.deepcopy(h41.DrawNormalized())

stack = ROOT.THStack("stack",";number of detectors;rel. number of entries")
stack2 = ROOT.THStack("stack",";number of detectors;number of entries")
stack.Add(h12,'HISTO')
stack.Add(h22,'HISTO')
# stack.Add(h32,'HISTO')
stack.Add(h42,'HISTO')
leg = style.make_legend(.3,.9,3)
leg.AddEntry(h12,'','l')
leg.AddEntry(h22,'','l')
# leg.AddEntry(h32,'','l')
leg.AddEntry(h42,'','l')
stack.Draw('nostack')
leg.Draw()
c.Update()
# c.BuildLegend()
c.Update()
style.save_canvas(c,'cluster_selection')

c2 = style.get_canvas('c2')
histos = [h11,h21,h41]
contents = map(lambda h:h.GetBinContent(h.GetXaxis().FindBin(8)),histos)
print contents
contents[0] -= contents[2]
contents[1] -= contents[2]
contributions= OrderedDict([
        ('at least 1 cluster', contents[0]),
        ('exactly 1 cluster', contents[1]),
        ('exaclty one valid cluster', contents[2])
    ]
)
colors= range(1,4)
pie = ROOT.TPie("cuts","cuts",len(contents), ar(contents, 'f'), ar(colors, 'i'))
pie.SetHeight(.04)
pie.SetRadius(.2)
for i, label in enumerate(contributions.iterkeys()):
    pie.SetEntryRadiusOffset(i, .05)
    pie.SetEntryLabel(i, label.title())
pie.Draw('rsc')

c2
