
import ROOT
import root_style
import copy
from copy import deepcopy

style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
style.set_style(800,800,1)

if True:
    f = ROOT.TFile.Open('input/3-radiation/cPedestalOfChannels_D2Y.root')
    style.set_style(800, 800, 1,min_marg_right=style.marg_left)
    cc = style.get_canvas('c')
    c2 = f.Get('cPedestalOfChannels_D2Y')
    h1 =  copy.deepcopy( c2.GetPrimitive('hMeanPedestal_Value_OfChannel_D2Y'))
    h2 = copy.deepcopy(c2.GetPrimitive('hMeanPedestal_Width_OfChannel_D2Y'))
    axis = None
    fr = None
    for p in c2.GetListOfPrimitives():
        if p.Class()==ROOT.TGaxis.Class():
            axis = copy.deepcopy(p)
        if p.Class()==ROOT.TFrame.Class():
            pass
            # fr = copy.deepcopy(p)
        if p.Class() == ROOT.TPaveText.Class():
            pt = copy.deepcopy(p)

    h1.SetTitle('mean pedestal')
    h1.GetXaxis().SetTitle('channel no.')
    h1.GetYaxis().SetTitle('mean pedestal / ADC')
    h2.SetTitle('mean noise')
    h2.GetYaxis().SetTitle('mean noise / ADC')
    axis.SetTitle('mean noise / ADC')
    for ax in [h1.GetXaxis(),h1.GetYaxis(),h2.GetYaxis(),axis]:
        ax.SetTitleFont(42)
        ax.SetLabelFont(42)
    cc.cd()
    h1.Draw()
    h2.Draw('same')
    cc.Update()
    axis.Draw('')

    leg = style.make_legend(.50,.95,2)
    leg.AddEntry(h1,'','l')
    leg.AddEntry(h2,'','l')
    leg.Draw()
    if pt:
        pt.GetLine(3).SetTitle('Plane D2Y')
        pt.SetTextFont(42)
        pt.SetFillStyle(0)
        #pt.Draw('same')
    # leg.Draw()
    style.save_canvas(cc,'pedestal_of_channel_d2y')
if False:
    c = style.get_canvas('c')
    f = ROOT.TFile.Open('input/3-radiation/histograms.pedestal.17000.root')
    c2 = f.Get('c_hNoiseDistributionOfAllNonHitChannels_D2Y')
    c.cd()
    h = copy.deepcopy(c2.GetPrimitive('hNoiseDistributionOfAllNonHitChannels_D2Y'))
    h.SetName('hNoise')
    h.SetStats(0)
    func = h.GetFunction('histofitx')
    func2 = func.Clone()
    func2.SetRange(-10,10)
    func2.SetLineStyle(2)

    # ROOT.gStyle.SetOptStat(1000000001)
    # ROOT.gStyle.SetOptFit(0011)
    style.adjust_histo_style(h)
    h.GetXaxis().SetTitle('noise / ADC')
    h.GetYaxis().SetTitle('number of entries')
    h.GetYaxis().SetTitleOffset(1.6)
    h.Draw()
    func2.Draw('same')
    pt = ROOT.TPaveText(.65,.75,.9,.9,'NDCNB')
    pt.SetTextAlign(12)
    pt.AddText('Fit:')
    pt.AddText('#bullet Mean:  %4.2f ADC'%func2.GetParameter(1))
    pt.AddText('#bullet Sigma: %3.2f ADC'%func2.GetParameter(2))
    for t in pt.GetListOfLines():
        t.SetTextAlign(12)
    pt.SetLineColor(0)
    pt.SetFillColor(0)
    pt.SetBorderSize(0)
    pt.SetFillStyle(0)
    pt.Draw()
    print c.GetWw(), c.GetWh()
    style.save_canvas(c,'pedestal_of_channel_d2y_dist')