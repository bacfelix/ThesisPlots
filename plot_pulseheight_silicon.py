
import ROOT
import root_style
import copy
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
c = style.get_canvas('c')


f = ROOT.TFile.Open('input/3-radiation/hPulseHeightDistribution_D2Y.root')
c2 = f.Get('cRoot_hPulseHeightDistribution_D2Y')
histo = c2.GetPrimitive('hPulseHeightDistribution_D2Y')
histo.RebinX(2)
histos = []
stack = ROOT.THStack('stack',';signal / ADC;number of entries')
i_max = 3
for i in range(1,i_max+1):
    bin = histo.GetYaxis().FindBin(i)
    if i< i_max:
        h = histo.ProjectionX('_%d'%i,bin,bin)
        title = 'cluster size = %d'%i
    else:
        h = histo.ProjectionX('_%d'%i,bin,100)
        title = 'cluster size > %d'%(i-1)
    h.SetTitle(title)
    h.SetLineColor(style.get_color(i))
    histos.append(h)
    stack.Add(histos[-1])
leg = style.make_legend(.48,.90,len(histos)*3+1,Y1=.5)

leg.SetEntrySeparation(.8)
hh = histo.ProjectionX('all',1,1)
leg.AddEntry(None,'Mean_{all}: %5.1f #pm %4.1f'%(hh.GetMean(),hh.GetRMS()),'')
print hh.GetXaxis().GetBinCenter(hh.GetMaximumBin())
for h in histos:
    leg.AddEntry(h,'','l')
    mean = '\tMean: %5.1f #pm %4.1f'%(h.GetMean(),h.GetRMS())
    mp = h.GetXaxis().GetBinCenter(h.GetMaximumBin())
    leg.AddEntry(None,mean,'')
    leg.AddEntry(None,'\tMP: %4.1f'%mp,'')
c.cd()
stack.Draw('nostack')
stack.GetXaxis().SetRangeUser(0,400)
stack.GetYaxis().SetRangeUser(.1,2e5)
stack.Draw('nostack')
leg.Draw()
c.SetLogy()
print leg.GetY1NDC()

leg.SetEntrySeparation(.5)
c.Modified()
c.Update()
leg2 = copy.deepcopy(leg)
style.save_canvas(c,'ph_vs_clustersize_sil')
#
# stack2 = ROOT.THStack('stack2',';signal / ADC;rel. number of entries')
# histos2 = []
# for h in histos:
#     hh = copy.deepcopy(h.DrawNormalized('goff'))
#     hh.Rebin(4)
#     print h,hh
#     histos2.append(hh)
#     stack2.Add(hh)
# stack2.Draw('nostack')
# stack2.GetXaxis().SetRangeUser(0,300)
# leg2.Draw()
# style.save_canvas(c,'ph_vs_clustersize_sil_rel')