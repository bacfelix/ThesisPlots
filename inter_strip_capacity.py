import math
import scipy
import scipy.special
from math import tanh, pi,sqrt

K = scipy.special.ellipk

def capacity(h,w,P,epsilon,l=1e-3):
    #spacing between strips: 2*g
    # g = (P-w)/2
    print "Pitch",P
    print "width",w
    print "spacing",P-w
    g = float(P-w)/2.
    s = float(w)
    k = tanh(pi*g/2./h)/tanh(pi*(s+g)/2./h)
    kP = sqrt(1-k**2)
    k0 = g/(s+g)
    k0P = sqrt(1-k0**2)
    q = 1./2.*K(kP)/K(k)*K(k0)/K(k0P)
    eps_eff = 1+(epsilon-1)*q
    C = 8.85e-12*eps_eff*K(k0P)/K(k0)
    print g,s,q,eps_eff,C
    return C*1e12*l


