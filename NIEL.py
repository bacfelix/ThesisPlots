__author__ = 'bachmair'
import ROOT
from numpy import array
import json
import root_style
from collections import OrderedDict
this_style = root_style.root_style()
style = root_style.root_style(batch=False)
style.set_main_dir('output/1-intro')
style.set_style(800,800,1)#1/1.2)
canvas = style.get_canvas('test')
canvas.cd()

f = 'NIEL.json'
output_file='NIEL'

entries = [u'Silicon_proton',
           u'Silicon_neutron',
           u'Silicon_PiPlus',
           u'Diamond_proton',
           u'Diamond_neutron',
           u'Diamond_PiPlus']
colors = [ROOT.kCyan+1,ROOT.kViolet,ROOT.kBlack,ROOT.kRed,ROOT.kGreen,ROOT.kBlue]
marker_styles = [25,21,20,2,5,3]
xytitles = ';energy / GeV ;NIEL / GeV/particle/cm^{3}'
def get_bias_scan_graph(name,data,marker_style=21,marker_color=ROOT.kBlack,line_color=ROOT.kBlack):
    x = [(float(i)) for i in data['x']]
    x = array(x,'f')
    y = [float(i) for i in data['y']]
    y = array(y,'f')
    print x, min(x), max(x)
    print y
    print len(x)
    g = ROOT.TGraph(len(x))
    for i  in range(len(x)):
        g.SetPoint(i,x[i],y[i])
        print i,x[i],y[i]
    g.SetName(name)
    g.SetTitle(data['title']+xytitles)
    g.SetMarkerColor(marker_color)
    g.SetLineColor(line_color)
    g.SetMarkerStyle(marker_style)
    g.SetMarkerSize(1.5)
    return g


fname = 'input/1-intro/' + f
print 'load', fname
graphs = OrderedDict()
mg = ROOT.TMultiGraph('mg',xytitles)

with open(fname) as data_file:
    all_data = json.load(data_file)
    all_data = all_data['wpd']
    all_data = all_data[u'dataSeries']
    for d in all_data:
        key = d['name']
        data = d['data']
        k = key.replace(', ','_')
        k = k.replace('+','Plus')

        print k
        x = [i['value'][0] for i in data]
        y = [i['value'][1] for i in data]
        for i in zip(x,y):
            print i
        # raw_input()
        data = {
            'x': x,
            'y': y,
            'title': key
        }
        i = entries.index(k)
        graph = get_bias_scan_graph('NIEL_'+k, data, marker_style=marker_styles[i],
                                    marker_color=colors[i],line_color=colors[i])
        graph.SetTitle(key)
        graphs[k] = graph
        mg.Add(graph,'LP')
leg = style.make_legend(.561,.94,len(graphs),X2=0.94)
leg.SetFillStyle(1001)
for g in graphs.values():
    leg.AddEntry(g,g.GetTitle(),'PL')
print graphs
frame = canvas.DrawFrame(0.01,0.00002,100,0.002,xytitles)
mg.Draw('')
leg.Draw()
# mg.GetXaxis().SetLimits(0.01,100)
# mg.GetYaxis().SetLimits(0.00001,0.002)
# graph.SetMinimum(1)
# graph.SetMaximum(1000)
# graph.GetXaxis().SetRangeUser(2000,2018)
frame.GetXaxis().SetNoExponent()
frame.GetYaxis().SetNoExponent()
frame.GetYaxis().SetNdivisions(10)
frame.GetYaxis().SetTitleOffset(1.9)
canvas.SetLogx()
canvas.SetLogy()
canvas.SetGridx()
canvas.SetGridy()
canvas.Update()
style.save_canvas(canvas,output_file)

