__author__ = 'bachmair'
import ROOT
import root_style
from numpy import array
this_style = root_style.root_style()
#//A run 1  t=535um big/grwth up, cut by II6, after RIE, SP
x25 = [0.,50.,100.,200.,300.,400.,500.,600.,700.,800.,900.,1000.]
y25=[6.7,47.6,77.0,145.5,190.8,225.3,257.5,275.5,293.7,307.9,320.1,325.7]
xerr25 = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
yerr25 = [2.0,4.5,6.0,10.,16.,20.,20.,20.,20.,20.,20.,20.]

    #//B run 1   t=535um big/grwth up, cut by II6, after RIE, SP
x26 = [0.,50.,100.,200.,300.,400.,500.,600.,700.,800.,900.,1000.]
y26 = [4.9,53.1,83.7,151.95,199.1,233.2,257.0,266.4,290.8,301.0,308.7,315.3]
xerr26 = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
yerr26 = [2.0,4.5,6.0,10.,16.,20.,20.,20.,20.,20.,20.,20.]

#//C run 1  t=535um big/grwth up, cut by II6, after RIE, SP
x27 = [0.,50.,100.,200.,300.,400.,500.,600.,700.,800.,900.,1000.]
y27 = [8.1,60.0,90.5,163.4,212.3,241.3,264.6,280.3,294.0,303.6,307.1,310.2]
xerr27 = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
yerr27 = [2.0,4.5,6.0,10.,16.,20.,20.,20.,20.,20.,20.,20.]

#//D run 1  t=535um big/grwth up, cut by II6, after RIE, SP
x28 = [0.,50.,100.,200.,300.,400.,500.,600.,700.,800.,900.,1000.]
y28 = [8.8,51.4,79.3,149.6,195.1,227.1,256.1,268.4,288.7,299.3,310.6,313.9]
xerr28 = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
yerr28 = [2.0,4.5,6.0,10.,16.,20.,20.,20.,20.,20.,20.,20.]
#
#//Correct for offsets
yoff25=6.7;
yoff26=4.9;
yoff27=8.1;
yoff28=8.8;

for i  in range(len(x25)):
    y25[i]=y25[i]-yoff25
    y26[i]=y26[i]-yoff26
    y27[i]=y27[i]-yoff27
    y28[i]=y28[i]-yoff28


style = root_style.root_style(batch=False)
style.set_main_dir('output/5-rate_studies')
style.set_style(1200,1000,1/1.2)
canvas = style.get_canvas('test')
canvas.cd()
xytitles = ';Bias Voltage / V;Charge Collection Distance / #mum'

g25 = ROOT.TGraphErrors(len(x25),array(x25,'f'),array(y25,'f'),array(xerr25,'f'),array(yerr25,'f'))
g25.SetName('PolyA')
g25.SetTitle('Final Part A: t = 535 #mum'+xytitles)
g25.SetMarkerColor(ROOT.kRed)
g25.SetMarkerStyle(20)
g25.SetMarkerSize(1.5)

g26 = ROOT.TGraphErrors(len(x26),array(x26,'f'),array(y26,'f'),array(xerr26,'f'),array(yerr26,'f'))
g26.SetName('PolyB')
g26.SetTitle('Final Part B: t = 535 #mum'+xytitles)
g26.SetMarkerColor(ROOT.kBlue)
g26.SetMarkerStyle(21)
g26.SetMarkerSize(1.5)

g27 = ROOT.TGraphErrors(len(x27),array(x27,'f'),array(y27,'f'),array(xerr27,'f'),array(yerr27,'f'))
g27.SetName('PolyC')
g27.SetTitle('Final Part C: t = 535 #mum'+xytitles)
g27.SetMarkerColor(ROOT.kMagenta)
g27.SetMarkerStyle(22)
g27.SetMarkerSize(1.5)

g28 = ROOT.TGraphErrors(len(x28),array(x28,'f'),array(y28,'f'),array(xerr28,'f'),array(yerr28,'f'))
g28.SetName('PolyD')
g28.SetTitle('Final Part D: t = 535 #mum'+xytitles)
g28.SetMarkerColor(ROOT.kBlack)
g28.SetMarkerStyle(23)
g28.SetMarkerSize(1.5)

mg = ROOT.TMultiGraph('mg',xytitles)
gs = [g25,g26,g27,g28]
for g in gs:
    g.Draw('APE')
    lab = ROOT.TPaveLabel(.81,.87,.93,.94,'RD42','NDC NB')
    lab.SetShadowColor(0)
    lab.SetFillStyle(1001)
    lab.SetFillColor(0)
    lab.SetLineColorAlpha(0,0)
    lab.SetTextFont(62)
    lab.Draw()
    # maxy = max([g.GetY()[i] for i in range(g.GetN())])
    # print maxy
    # maxy*=1.2
    g.SetMaximum(400)
    canvas.SetGridx()
    canvas.SetGridy()
    canvas.Update()
    style.save_canvas(canvas,'biasScan_'+g.GetName())
    mg.Add(g,'PE')

mg.Draw('A')
mg.SetMaximum(500)
canvas.Update()
mg.Draw('A')
# mg.GetXaxis().SetAxisColor(17)
# mg.GetYaxis().SetAxisColor(17)
# canvas.RedrawAxis()
leg = style.make_legend(.15,.95,4)
ggs = []
for g in gs:
    gg = g.Clone()
    gg.SetMarkerSize(3)
    gg.SetMarkerColor(g.GetMarkerColor())
    gg.SetMarkerStyle(g.GetMarkerStyle())
    l = leg.AddEntry(gg,g.GetTitle().split(';')[0],'P')
    l.SetTextColor(g.GetMarkerColor())
    ggs.append(gg)
leg.Draw()
canvas.SetGridx()
canvas.SetGridy()
canvas.Update()
lab = ROOT.TPaveLabel(.81,.87,.93,.94,'RD42','NDC NB')
lab.SetShadowColor(0)
lab.SetFillStyle(1001)
lab.SetFillColor(0)
lab.SetLineColorAlpha(0,0)

lab.SetTextFont(62)
lab.Draw()
canvas.Update()
style.save_canvas(canvas,'biasScan_II-VI-pCVDs')

