import ROOT
import pickle
import os
from math import sqrt
from root_style import *



def get_file_names(runnumber):
    fdir = './input/3-radiation/'
    return {
        'pedestal': fdir+'pedestalData.%d.root'%runnumber,
        'raw': fdir+'rawData.%d.root'%runnumber,
    }

def mean(l):
    return reduce(lambda x,y:x+y,l)/float(len(l))

def mean_and_sigma(l):
    mean_val = mean(l)
    l2 = map(lambda x:x**2,l)
    m2 = mean(l2)
    sigma = sqrt(m2 - mean_val**2)
    return mean_val,sigma

def get_cutted_list(l,m,s,ns):
    return filter(lambda x:abs((x-m)/s)<ns,l)

def calculate_pedestals(l,ns=5,bPrint=False):
    k = 0
    m,s = mean_and_sigma(l)
    while True:
        try:
            l2 = get_cutted_list(l,m,s,ns)
        except:
            k = -1
            break
        if bPrint: print k,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
        if len(l2) == len(l):
            break
        l = l2
        m,s = mean_and_sigma(l)
        k += 1
    return k,len(l),m,s

def get_pedestals(raws):
    ped = {}
    for pl, raw_ch in raws.iteritems():
        ped[pl] = []
        for ch  in range(len(raw_ch)):
            values = [pl,ch]
            values.extend(calculate_pedestals(raw_ch[ch]))
            ped[pl].append(values)
    return ped

def get_raws(tree,n = 500):
    print 'Get first %d RawValues of %s'%(n,tree)
    ped = {}
    for plane in range(8):
        ped[plane] = []
        for ch in range(256):
            ped[plane].append([])
    ped[8] = []
    for ch in range(128):
        ped[8].append([])
    for i in range(0,n):
        tree.GetEntry(i)
        for ch in range(0,256):
            ped[0][ch].append(ord(tree.D0X_ADC[ch]))
            ped[1][ch].append(ord(tree.D1X_ADC[ch]))
            ped[2][ch].append(ord(tree.D2X_ADC[ch]))
            ped[3][ch].append(ord(tree.D3X_ADC[ch]))
            ped[4][ch].append(ord(tree.D0Y_ADC[ch]))
            ped[5][ch].append(ord(tree.D1Y_ADC[ch]))
            ped[6][ch].append(ord(tree.D2Y_ADC[ch]))
            ped[7][ch].append(ord(tree.D3Y_ADC[ch]))
        for ch in range(0,128):
            ped[8][ch].append(tree.DiaADC[ch])
    return ped
runnumbers = []
for item in os.listdir('.'):
    if os.path.isdir(item):
        try:
            runnumber = int(item)
            if runnumber/100000 == 0:
                runnumbers.append(runnumber)
        except:
            pass

def draw_pedestals(tree,c1,c2,event):
    print 'Event %d'%event
    tree.GetEntry(event)
    tree.GetListOfFriends().Print()
    for tf in tree.GetListOfFriends():
        tf.SetTitle(get_file_names(runnumber)['raw'])
    adcs = [ord(tree.D2Y_ADC[ch]) for ch in range(256)]
    pedestals = [tree.PedestalMean[5*256+ch] for ch in range(256)]
    sigmas = [tree.PedestalSigma[5*256+ch] for ch in range(256)]
    signals = map(lambda x,y: x-y,adcs,pedestals)
    snrs = map(lambda x,y:x/y, signals,sigmas)
    print snrs
    create_histo(adcs,'h_adc',256,0,256)
    h_adc = ROOT.TH1F('h_adc','ADC;channel no;ADC',256,0,256)
    h_ped = ROOT.TH1F('h_ped','pedestal;channel no;ADC',256,0,256)
    h_sig = ROOT.TH1F('h_sig','signal;channel no;ADC',256,0,256)
    h_pw1 = ROOT.TH1F('h_pw1','h_pw1',256,0,256)
    h_pw5 = ROOT.TH1F('h_pw5','h_pw5',256,0,256)
    h_pw10 = ROOT.TH1F('h_pw10','h_pw10',256,0,256)
    h_snr = ROOT.TH1F('h_snr','SNR;channel no;SNR',256,0,256)
    for i in range(0,256):
        h_adc.SetBinContent(i+1,adcs[i])
        h_ped.SetBinContent(i+1,pedestals[i])
        h_sig.SetBinContent(i+1,signals[i])
        h_snr.SetBinContent(i+1,snrs[i])
        h_pw1.SetBinContent(i+1,sigmas[i])
        h_pw5.SetBinContent(i+1,sigmas[i]*5)
        h_pw10.SetBinContent(i+1,sigmas[i]*10)

    c1.cd()
    h_adc.Draw()
    h_ped.SetLineColor(ROOT.kRed)
    h_ped.Draw('same')
    leg = style.make_legend(.7,.9,2)
    leg.AddEntry(h_adc,'','l')
    leg.AddEntry(h_ped,'','l')
    leg.Draw()
    # c.cd(2)
    # h_sig.Draw()
    # h_pw1.SetLineColor(ROOT.kBlue)
    # h_pw1.Draw('same')
    c2.cd()
    h_snr.Draw()
    seed = ROOT.TCutG('seed',2)
    hit = ROOT.TCutG('hit',2)
    zero = ROOT.TCutG('zero',2)
    zero.SetPoint(0,-1e9,0)
    zero.SetPoint(1,+1e9,0)
    seed.SetPoint(0,-1e9,10)
    seed.SetPoint(1,+1e9,10)
    hit.SetPoint(0,-1e9,5)
    hit.SetPoint(1,+1e9,5)
    seed.SetLineColor(ROOT.kRed)
    seed.SetLineStyle(2)
    zero.SetLineStyle(2)
    hit.SetLineColor(ROOT.kBlue)
    hit.SetLineStyle(2)
    seed.SetTitle('seed cut')
    hit.SetTitle('hit cut')
    seed.Draw('same')
    hit.Draw('same')
    zero.Draw('same')
    leg2 = style.make_legend(.7,.9,3)
    leg2.AddEntry(h_snr,'','l')
    leg2.AddEntry(seed,'','l')
    leg2.AddEntry(hit,'','l')
    leg2.Draw()
    c1.Update()
    c2.Update()
    ri = raw_input()
    if(  ri == 'y'):
        style.save_canvas(c1,'pedestal_1_%d'%event)
        style.save_canvas(c2,'pedestal_SNR_%d'%event)
    elif ri == 'n':
        return False
    return True


runnumber = 17000
event = 1000

style  = root_style(batch=False)
style.set_main_dir('output/3-radiation/')
c1 = style.get_canvas('c1')
c2 = style.get_canvas('c2')
# c.Divide(,1)
fnames =  get_file_names(runnumber)
fname = fnames['pedestal']
print fnames
print fname
f = ROOT.TFile.Open(fname)
tree = f.Get('pedestalTree')
while True:
    retval = draw_pedestals(tree,c1,c2,event)
    if retval == False:
        break
    event += 1

#
# def calculate_pedestals(l,ns=5,k=5):
#     for i in range(k):
#         m,s = mean_and_sigma(l)
#         l2 = get_cutted_list(l,m,s,5)
#         print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
# calculate_pedestals(ped[0])
# def calculate_pedestals(l,ns=5,k=5):
#     for i in range(k):
#         m,s = mean_and_sigma(l)
#         l2 = get_cutted_list(l,m,s,5)
#         print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
#         l = l2
# calculate_pedestals(ped[0])
# calculate_pedestals(ped[0],n2=4)
# calculate_pedestals(ped[0],ns=4)
# calculate_pedestals(ped[0],ns=3)
# calculate_pedestals(ped[0],ns=2)
# calculate_pedestals(ped[0],ns=1)
# calculate_pedestals(ped[0],ns=0)
# def calculate_pedestals(l,ns=5,k=5):
#     for i in range(k):
#         m,s = mean_and_sigma(l)
#         l2 = get_cutted_list(l,m,s,ns)
#         print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
#         l = l2
# calculate_pedestals(ped[0],n2=4)
# calculate_pedestals(ped[0],ns=5)
# calculate_pedestals(ped[0],ns=4)
# calculate_pedestals(ped[0],ns=3)
# calculate_pedestals(ped[0],ns=2)
# calculate_pedestals(ped[0],ns=1)
# calculate_pedestals(ped[0],ns=5)
# def calculate_pedestals(l,ns=5,k=5):
#     for i in range(k):
#         m,s = mean_and_sigma(l)
#         l2 = get_cutted_list(l,m,s,ns)
#         print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
#         l = l2
#     m,s = mean_and_sigma(l)
#     return len(l),m,s
# calculate_pedestals(ped[0],ns=5)
# def calculate_pedestals(l,ns=5,k=5,bPrint=True):
#     for i in range(k):
#         m,s = mean_and_sigma(l)
#         l2 = get_cutted_list(l,m,s,ns)
#         if bPrint: print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
#         l = l2
#     m,s = mean_and_sigma(l)
#     return len(l),m,s
# calculate_pedestals(ped[0],ns=5)
# calculate_pedestals(ped[0],ns=5,False)
# calculate_pedestals(ped[0],ns=5,bPrint= False)
# [500-calculate_pedestals(ped[ch],ns=5,bPrint= False)[0] for ch in range(len(ped))]
# retVals = [500-calculate_pedestals(ped[ch],ns=5,bPrint= False)[0] for ch in range(len(ped))]
# max(retVals)
# min(retVals)
# retVals.index(20)
# calculate_pedestals(ped[151])
# calculate_pedestals(ped[151],ns5,k=10)
# calculate_pedestals(ped[151],ns=5,k=10)
# calculate_pedestals(ped[151],ns=5,k=10)
# def calculate_pedestals(l,ns=5,k=5,bPrint=True):
#     for i in range(k):
#         m,s = mean_and_sigma(l)
#         l2 = get_cutted_list(l,m,s,ns)
#         if bPrint: print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
#         l = l2
#     m,s = mean_and_sigma(l)
#     return len(l),m,s
# def calculate_pedestals(l,ns=5,bPrint=True):
#     m,s = mean_and_sigma(l)
#     while True:
#         l2 = get_cutted_list(l,m,s,ns)
#         if bPrint: print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
#         if len(l2) = len(l):
#             break
#         l = l2
#         m,s = mean_and_sigma(l)
#         k += 1
# calculate_pedestals(ped[151],ns=5)
# retVals = [calculate_pedestals(ped[ch],ns=5,bPrint= False)[0] for ch in range(len(ped))]
# retVals
# max(retVals)
# min(retVals)
# retVals = [calculate_pedestals(ped[ch],ns=5,bPrint= False)[0] for ch in range(len(ped))]
# retVals = [calculate_pedestals(ped[ch],ns=5,bPrint= False) for ch in range(len(ped))]
# retVals = [calculate_pedestals(ped[ch],ns=5,bPrint= False)[0] for ch in range(len(ped))]
# retVals.index(5)
# retVals.index(3)
# def indices(lst, element):
#         result = []
#         offset = -1
#         while True:
#                 try:
#                         offset = lst.index(element, offset+1)
#                     except ValueError:
#                             return result
#                     result.append(offset)
# def indices(lst, element):
#         result = []
#         offset = -1
#         while True:
#             try:
#                 offset = lst.index(element, offset+1)
#             except ValueError:
#                 return result
#             result.append(offset)
# indices(retVals,3)
# len(indices(retVals,3))
# len(indices(retVals,0))
# len(indices(retVals,1))
# len(indices(retVals,2))
# len(indices(retVals,3))
# len(indices(retVals,4))
# len(indices(retVals,5))
# def calculate_pedestals(l,ns=5,bPrint=True):
#     k = 0
#     m,s = mean_and_sigma(l)
#     while True:
#         l2 = get_cutted_list(l,m,s,ns)
#         if bPrint: print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
#         if len(l2) == len(l):
#             break
#         l = l2
#         m,s = mean_and_sigma(l)
#         k += 1
#     return k,len(l),m,s
# def calculate_pedestals(l,ns=5,bPrint=True):
#     k = 0
#     m_start, s_start = mean_and_sigma(l)
#     m = m_start
#     s = s_start
#     while True:l2 = get_cutted_list(l,m,s,ns)
# def calculate_pedestals(l,ns=5,bPrint=True):
#     k = 0
# def calculate_pedestals(l,ns=5,bPrint=True):
#     k = 0
#     m_start, s_start = mean_and_sigma(l)
#     m = m_start
#     s = s_start
#     while True:l2 = get_cutted_list(l,m,s,ns)
#         l2 = get_cutted_list(l,m,s,ns)
#         if bPrint: print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
# def calculate_pedestals(l,ns=5,bPrint=True):
#     k = 0
#     m_start, s_start = mean_and_sigma(l)
#     m = m_start
#     s = s_start
#     while True:
#         l2 = get_cutted_list(l,m,s,ns)
#         if bPrint: print i,len(l),'-->',len(l2), '%6.3f +/- %5.3f'%(m,s)
#         if len(l2) == len(l):
#             break
#         l = l2
#         m,s = mean_and_sigma(l)
#         k += 1
#     return k,len(l),m,s,m_start,s_start
# calculate_pedestals(ped[151],ns=5)
# retVals = [calculate_pedestals(ped[ch],ns=5,bPrint= False) for ch in range(len(ped))]
# retVals[0]
# retVals[0][2]-retVals[4]
# retVals[0][2]-retVals[0][4]
# delta_p = [x[2]-x[4] for x in retVals]
# max(delta_p)
# min(delta_p)
# delta_p = [x[2]-x[4] for x in retVals]
# delta_s = [x[3]-x[5] for x in retVals]
# max(delta_s)
# min(delta_s)
# for i in range(0,500):#tree.GetEntries()):
#     tree.GetEntry(i)
#     for ch in range(0,256):
#         ped[ch].append(ord(tree.D0X_ADC[ch]))
# ped = [[]]
# ped = [[]]*256
# ped = [[[]]*256]*8
# ped
# for i in range(0,500):
#     tree.GetEntry(i)
#     for ch in range(0,256):
#         ped[0][ch].append(ord(tree.D0X_ADC[ch]))
#         ped[1][ch].append(ord(tree.D1X_ADC[ch]))
#         ped[2][ch].append(ord(tree.D2X_ADC[ch]))
#         ped[3][ch].append(ord(tree.D3X_ADC[ch]))
#         ped[4][ch].append(ord(tree.D0Y_ADC[ch]))
#         ped[5][ch].append(ord(tree.D1Y_ADC[ch]))
#         ped[6][ch].append(ord(tree.D2Y_ADC[ch]))
#         ped[7][ch].append(ord(tree.D3Y_ADC[ch]))
# retVals = [calculate_pedestals(ped[0][ch],ns=5,bPrint= False) for ch in range(len(ped[0]))]
# retVals = [calculate_pedestals(ped[0][ch],ns=5,bPrint= False) for ch in range(len(ped[0][0]))]
# len(ped)
# len(ped[0])
# len(ped[0][0])
# ped
# len(ped[0][0])
# retVals = [calculate_pedestals(ped[0][ch],ns=5,bPrint= False) for ch in range(len(ped[0]))]
# retVals
# max(retVals[][0])
# max(retVals[:][0])
# max(retVals[:][1])
# retVals[:][0]
# retVals[:]
# retVals[:][:][0]
# [x[0] for x in retVals]
# retVals = [calculate_pedestals(ped[p][ch],ns=5,bPrint= False) for ch in range(len(ped[p])) for p in range(8)]
# retVals = [[calculate_pedestals(ped[p][ch],ns=5,bPrint= False) for ch in range(len(ped[p]))] for p in range(8)]
# retVals
# for pl in range(8):
#     max([x[0] for x in retVals[pl]])
# for pl in range(8):
#     print max([x[0] for x in retVals[pl]])
# for pl in range(8):
#     print min([(x[1]) for x in retVals[pl]])
# for pl in range(8):
#     print min([abs(x[2]-x[3]) for x in retVals[pl]])
# for pl in range(8):
#     print min([abs(x[2]-x[4]) for x in retVals[pl]])
# for pl in range(8):
#     print min([abs(x[3]-x[5]) for x in retVals[pl]])
# for pl in range(8):
#     print max([abs(x[3]-x[5]) for x in retVals[pl]])
# for pl in range(8):
#     print max([abs(x[2]-x[4]) for x in retVals[pl]])
# n_channels = []
# for pl in range(8):
#     n_channels.extend([x[1] for x in retVals[pl]])
# n_channels
# histo = ROOT.TH1F("histo","histo",501,-.5,500.5)
# for c in n_channels:
#     histo.Fill(c)
# histo.Draw()
# def create_histo(l,bins,low,up,name='histo'):
#     histo = ROOT.TH1F(name,name,bins,low,up)
#     for c in l:
#         histo.Fill(c)
#     return c
# create_histo(l,5001,-.5,500.5,"h_used_events")
# def create_histo(l,bins,low,up,name='histo'):
#     histo = ROOT.TH1F(name,name,bins,low,up)
#     for c in l:
#         histo.Fill(c)
#     return histo
# def create_histo(l,bins,low,up,name='histo'):
#     histo = ROOT.TH1F(name,name,bins,low,up)
#     for c in l:
#         histo.Fill(c)
#     return histo
# h = create_histo(l,5001,-.5,500.5,"h_used_events")
# l
# h = create_histo(l,5001,-.5,500.5,"h_used_events")
# h.Draw()
# h = create_histo(n_channels,5001,-.5,500.5,"h_used_events")
# h = create_histo(n_channels,5001,-.5,500.5,"h_used_events")
# h.Draw()
# h = create_histo(n_channels,501,-.5,500.5,"h_used_events")
# h.Draw()
# h.GetXaxis().SetTitle('used Channels')
# n_channels = map(lambda x:x/500., n_channels)
# h = create_histo(n_channels,500,0,1,"h_used_events")
# h.Draw()
# n_channels = map(lambda x:x*100, n_channels)
# h = create_histo(n_channels,500,0,1,"h_used_events")
# h = create_histo(n_channels,500,0,100,"h_used_events")
# h.Draw()
# n_channels = []
# for pl in range(8):
#     n_channels.extend([x[1] for x in retVals[pl]])
# n_channels_rel = map(lambda x: x/5., n_channels)
# h = create_histo(n_channels_rel,500,0,100,"h_used_events")
# h.Draw()
# h.SaveAs('n_Channels_rel.root')
# n_iterations = []
# for pl in range(8):
#     n_iterations.extend([x[0] for x in retVals[pl]])
# h = create_histo(n_iterations,10,0,10,"h_n_iterations")
# h.Draw()
# h.SaveAs('n_iterations.root')
# n_delta_p = []
# for pl in range(8):
#     n_delta_p.extend([x[2]-x[4] for x in retVals[pl]])
# n_delta_s = []
# for pl in range(8):
#     n_delta_s.extend([x[3]-x[5] for x in retVals[pl]])
# len(n_delta_p)
# len(n_delta_s)
# delta_p=n_delta_p)
# delta_p=n_delta_p
# delta_s=n_delta_s
# min(delta_s)
# max(delta_s)
# max(delta_p)
# max(delta_s)
# min(delta_p)
# max(delta_p)
# h = create_histo(delta_p,100,-3,1,'delta_p')
# h.Draw()
# h.SaveAs('delta_p.root')
# h = create_histo(delta_s,100,-20,0,'delta_s')
# h.SaveAs('delta_s.root')
# h.Draw()
# h = ROOT.TH2F('p_vs_s','p_vs_s',100,-3,1,100,-20,0)
# for i in range(2048):
#     h.Fill(delta_p[i],delta_s[i])
# h.Draw('colz')
# h.SaveAs('p_vs_s.root')
#  h = ROOT.TH2F('s_vs_it',100,-20,0,10,0,10)
#  h = ROOT.TH2F('s_vs_it','s_vs_it',100,-20,0,10,0,10)
# for i in range(2048):
#     h.Fill(delta_p[i],n_iterations[i])
# h.Draw()
# h.Draw('colz')
# h.Reset()
# for i in range(2048):
#     h.Fill(n_iterations[i],delta_p[i])
# h.Draw()
# h.Draw('colz')
# h.Reset()
# for i in range(2048):
#     h.Fill(delta_p[i],n_iterations[i])
# h.Draw('colz')
# h.Reset()
# for i in range(2048):
#     h.Fill(delta_s[i],n_iterations[i])
# h.SaveAs('s_vs_it.root')
# %hisotry
# %history
# %history -f 'first_noise_estimate.py'
# %history -f first_noise_estimate.py
