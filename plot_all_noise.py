__author__ = 'bachmair'
from numpy import array
import json
import root_style
import ROOT
style = root_style.root_style()
chapter = '3-radiation'
main_dir = 'output/%s'%chapter
style.set_main_dir(main_dir)
style.set_style(800,800,1)#1/1.2)
ROOT.gROOT.SetBatch(False)

f = 'noise_data.json'
output_file='noise_data'
fname = 'input/%s/'%chapter + f
print 'load', fname
with open(fname) as data_file:
    data = json.load(data_file)
noise = []
cmc_noise = []
for dia in data:
    if 'cmc_noise'in data[dia]:
        cmc_noise.extend(data[dia]['cmc_noise'])
    if 'noise' in data[dia]:
        noise.extend(data[dia]['noise'])
print 'cmc', cmc_noise
print 'noise', noise
xmin = min(min(noise),min(cmc_noise))
xmax = max(max(noise),max(cmc_noise))
print xmin,xmax
xmin = 50
xmax = 190
xbins = 140
title = ';noise / e; number of entries'
h_raw  = ROOT.TH1F('hRaw','raw noise'+title,xbins,xmin,xmax)
h_raw.SetLineColor(ROOT.kBlue)
h_cmc  = ROOT.TH1F('hCmc','cmc noise'+title,xbins,xmin,xmax)
for n in noise:
    h_raw.Fill(n)
for n in cmc_noise:
    h_cmc.Fill(n)
leg_raw = style.make_legend(.6,.93,3)
leg_cmn = style.make_legend(.50, .93, 3)

stack  = ROOT.THStack('stack',title)
leg = style.make_legend(.50,.93,5)
stack.Add(h_raw)
for l in [leg,leg_raw]:
    l.AddEntry(h_raw,'raw noise','l')
    l.AddEntry(None,'','')
    l.AddEntry(None,'   Mean:','')
    l.AddEntry(None,'%4.1f e'%h_raw.GetMean(),'')
    l.AddEntry(None,'   RMS:','')
    l.AddEntry(None,' %4.1f e'%h_raw.GetRMS(),'')
stack.Add(h_cmc)
for l in [leg,leg_cmn]:
    l.AddEntry(h_cmc,'common mode corrected','l')
    l.AddEntry(None,'','')
    l.AddEntry(None,'   Mean:','')
    l.AddEntry(None,'%4.1f e'%h_cmc.GetMean(),'')
    l.AddEntry(None,'   RMS:', '')
    l.AddEntry(None, ' %4.1f e'%h_cmc.GetRMS(),'')
leg.SetNColumns(2)
leg_cmn.SetNColumns(2)
leg_raw.SetNColumns(2)

xmin = 50
xmax = 190
canvas2 = style.get_canvas('test2')
canvas2.cd()
stack.Draw('nostack')
stack.GetXaxis().SetLimits(xmin,xmax)
stack.GetXaxis().SetRangeUser(xmin,xmax)
leg.Draw()
style.save_canvas(canvas2,output_file+"_all")
ymax = stack.GetMaximum()
print ymax
ymax *= 1.1


xmin = 60
xmax = 150
canvas = style.get_canvas('test')
canvas.cd()
canvas.cd()
h_cmc.SetMaximum(ymax)
h_cmc.Draw()
h_cmc.GetXaxis().SetRangeUser(xmin,xmax)
leg_cmn.Draw()
canvas.Update()
style.save_canvas(canvas,output_file+"_cmc")

canvas1 = style.get_canvas('test1')
canvas1.Clear()
canvas1.cd()
h_raw.SetMaximum(ymax)
h_raw.Draw()
h_raw.GetXaxis().SetRangeUser(xmin,xmax)
leg_raw.Draw()
canvas1.Update()
style.save_canvas(canvas1,output_file+"_raw")