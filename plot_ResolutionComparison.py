from math import sqrt
import ROOT
import root_style
import copy
import time
from copy import deepcopy
from collections import OrderedDict
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/ResolutionComparison')
style.rel_marg_bot += .0
style.rel_marg_right = .0
style.set_style(1200,800,1./1.5)
c = style.get_canvas('c')
fdir = 'input/3-radiation/'
fname = 'histograms.transparent.{runno}.root'
# runs = [19109,
runs = [160011]
# runs = [16302,163021]
hnames = OrderedDict([
    ('max', 'hDiaTranspAnaResidualHighestHitIn10StripsMinusPred'),
    ('CW', 'hDiaTranspAnaResidualChargeWeightedIn10StripsMinusPred'),
    ('h2C', 'hDiaTranspAnaResidualHighest2CentroidIn10StripsMinusPred'),
    ('eta','hDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred'),
    # 'eta_pulseheight','hDiaTranspAnaPulseHeightOf2HighestIn10Strips'
])
legend_titles ={
    'max': 'Maximum ',
    'CW': 'Charge weighted ',
    'h2C': 'Highest 2 centroid ',
    'eta': 'Eta corrected '
}
histos = {}
for key in hnames:
    histos[key] = {}
def create_mirrored_distribution(h):
    h2 = h.Clone()
    nbins = h.GetNbinsX()+1
    for bin in range(1,nbins):
        content = h.GetBinContent(nbins-bin)
        h2.SetBinContent(bin,content)
    h2.SetLineColor(ROOT.kBlue)
    h2.SetStats(False)
    h.GetListOfFunctions().Clear()
    return h2
for key,hname in hnames.items():
    for run in runs:
        fpath = fdir+fname.format(runno=run)
        print fpath
        f = ROOT.TFile.Open(fpath)
        print fpath
        h =None
        for k in f.GetListOfKeys():
            if hname == k.GetName():
                h = f.Get(k.GetName())
                break
        if h == None:
            for k in f.GetListOfKeys():
                if "c_"+hname in k.GetName():
                    cc = f.Get(k.GetName())
                    print cc,k
                    break
            hh = cc.GetPrimitive(hname).Clone()
            h = deepcopy(hh)
        c.cd()
        h.UseCurrentStyle()
        h.GetListOfFunctions().Clear()
        h.GetXaxis().SetTitleOffset(1.5)
        if key == 'eta_distribution':
            h.GetXaxis().SetTitle('#eta = #frac{S_{R}}{S_{L} + S_{R}}')

        elif 'Residual' in hname:
            h.GetXaxis().SetTitle('residual / #mum')
            h.GetListOfFunctions().Clear()
            while h.GetXaxis().GetBinWidth(2) <.3:
                h.Rebin()
            # h.GetXaxis().SetRangeUser(-20, 20)
        print h.GetXaxis().GetBinWidth(2)
        h.GetYaxis().SetTitle('number of entries')
        h.Draw()
        if key == 'eta_distribution':
            h2 = create_mirrored_distribution(h)
            h2.Draw('same')
        c.Update()
        style.save_canvas(c,'{key}.{run}'.format(run=run,key=key))
        histos[key][run] = deepcopy(h)
sigma_tel = 4
if True:
    stacks = []
    keys = hnames.keys()
    xytitle = ';residual / #mum;number of entries'
    stack = ROOT.THStack('stack', xytitle)
    leg_pos = .65
    leg = style.make_legend(leg_pos, .92, len(keys)*1.5)

    for k in keys:
        i = 1
        for runno,h in sorted(histos[k].items()):
            h.SetLineColor(style.get_next_color())
            h.GetXaxis().SetRangeUser(-30,30)
            # h.Scale(1./h.GetBinContent(h.GetMaximumBin()))
            h.SetTitle(legend_titles[k])
            rms = h.GetRMS()
            fwhm  = root_style.get_fwhm(h)
            print '%20s & %5.2f & %5.2f & %5.2f \\\\ '%(h.GetTitle(),rms,sqrt(rms**2-sigma_tel**2),fwhm)
            h.GetXaxis().SetRangeUser(-50,50)
            leg.AddEntry(h,h.GetTitle(),'l')
            leg.AddEntry(None,'RMS_{%s} = %3.1f #mum'%(k,rms),'')
            stack.Add(h)
        stack.Draw('nostack')
        # if k == 'eta_resolution':
        stack.GetXaxis().SetRangeUser(-100,100)
        leg.Draw()
    # c.SetGridx()
    leg.SetFillStyle(1001)
    leg.SetLineColor(0)
    stack.GetXaxis().SetNdivisions(505)
    style.save_canvas(c,'ResolutionComparison')

    stacks.append(stack)

#
# fdir = 'input/3-radiation/'
# fname = 'correction.root'
#
# f = ROOT.TFile.Open(fdir+fname  )
# h = f.Get('h')
# h.UseCurrentStyle()
# h.GetXaxis().SetTitle('#frac{PH_{corrected}}{PH_{original}}')
# h.GetYaxis().SetTitle('number of entries')
# h.Draw()
# h.GetXaxis().SetTitleOffset(1.5)
# style.save_canvas(c,'pulseheight_correction')
#

