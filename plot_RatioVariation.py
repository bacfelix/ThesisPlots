
import ROOT
from ROOT import TCutG
import root_style
import copy
import numpy as np
def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
c = style.get_canvas('c')

ratio = 1.47
f = ROOT.TFile.Open('input/3-radiation/RatioVariation_ratio_1.47.root')
c2 = f.Get('ratioVariation')
for p in c2.GetListOfPrimitives():
    print p
g = c2.GetPrimitive('gRatioVariation')
x = [g.GetX()[i] for i in range(g.GetN())]
y = [g.GetY()[i] for i in range(g.GetN())]
pos = x.index(1.0)
yy = (y[pos-1]+y[pos+1])/2.
g.SetPoint(pos,x[pos],yy)

x_low = x.index(find_nearest(np.array(x),ratio*.5))
x_up = x.index(find_nearest(np.array(x),ratio*1.5))

print x_low,x_up
ymax = max(y[x_low:x_up+1])
ymin = min(y[x_low:x_up+1])
print ymax,ymin
g_xlow = TCutG('g_xlow',2)
g_xlow.SetPoint(0,.5*ratio,-1e9)
g_xlow.SetPoint(1,.5*ratio,+1e9)
g_xup  = TCutG('g_xup' ,2)
g_xup.SetPoint(0,1.5*ratio,-1e9)
g_xup.SetPoint(1,1.5*ratio,+1e9)
g_ylow = TCutG('g_ylow',2)
g_ylow.SetPoint(0,-1e9,ymin)
g_ylow.SetPoint(1,+1e9,ymin)
g_yup  = TCutG('g_yup' ,2)
g_yup.SetPoint(0,-1e9,ymax)
g_yup.SetPoint(1,+1e9,ymax)
g_xmid  = TCutG('g_xmid' ,2)
g_xmid.SetPoint(0,1*ratio,-1e9)
g_xmid.SetPoint(1,1*ratio,+1e9)
cuts = [g_yup,g_xlow,g_xup,g_ylow]

c.cd()
g.Draw('AP')
g.GetXaxis().SetTitle('MFP ratio #frac{#lambda_{h}}{#lambda_{e}}')
g.GetYaxis().SetTitle('damage constant k_{mfp} #times 10^{-18} / #mum^{-1}cm^{-2}')
for gg in cuts:
    if 'g_x' in gg.GetName():
        gg.SetLineColor(ROOT.kGreen)
    else:
        gg.SetLineColor(ROOT.kRed)
    gg.SetLineStyle(2)
    gg.Draw('same')
g_xmid.SetLineColor(ROOT.kBlue)
# g_xmid.SetLineStyle()
g_xmid.SetLineWidth(2)
g_xmid.Draw('same')
# leg.SetEntrySeparation(.5)
c.Modified()
c.Update()
# leg2 = copy.deepcopy(leg)
style.save_canvas(c,'800MeVprotons_RatioVariation_ratio_1.47')
#
# stack2 = ROOT.THStack('stack2',';signal / ADC;rel. number of entries')
# histos2 = []
# for h in histos:
#     hh = copy.deepcopy(h.DrawNormalized('goff'))
#     hh.Rebin(4)
#     print h,hh
#     histos2.append(hh)
#     stack2.Add(hh)
# stack2.Draw('nostack')
# stack2.GetXaxis().SetRangeUser(0,300)
# leg2.Draw()
# style.save_canvas(c,'ph_vs_clustersize_sil_rel')