import ROOT
import root_style
import copy
from array import array
import math

style = root_style.root_style(batch=True)
style.set_main_dir('output/3-radiation/asymmetric_eta/')
style.set_style(1400, 1000, 1 / 1.4)
c = style.get_canvas('c')

runs = [12012, 120121, 13014, 130140, 16302, 16303, 17000, 17305, 173050, 18003]
all_fits = {}
for run in runs:
    print run
    try:
        f = ROOT.TFile.Open('input/3-radiation/histograms.%d.root' % run)
        if False:
            cL = f.Get('cRoot_hLeftVsMaximumDia')
            cR = f.Get('cRoot_hRightVsMaximumDia')
            hL = copy.deepcopy(cL.GetPrimitive('hLeftVsMaximumDia'))
            hR = copy.deepcopy(cR.GetPrimitive('hRightVsMaximumDia'))
            hDiff = copy.deepcopy(hR.Clone('hDiff'))
            hDiff.SetTitle('Difference h_R - h_L')
            hDiff.GetXaxis().SetTitle('Difference S_R  - S_L')
            hDiff.Reset()
            for xbin in range(hDiff.GetNbinsX()):
                for ybin in range(hDiff.GetNbinsY()):
                    l = hL.GetBinContent(xbin, ybin)
                    r = hR.GetBinContent(xbin, ybin)
                    hDiff.SetBinContent(xbin, ybin, r - l)
            #
            # hDiff.Add(hL,1.)
            # hDiff.Add(hR,-1.)

            ncontours = 999
            MyPalette = []
            stops = [0.00, 0.50, 1.00]
            red = [0.00, 1.00, 1.00]
            green = [0.00, 1.00, 0.00]
            blue = [1.00, 1.00, 0.00]

            st = array('d', stops)
            re = array('d', red)
            gr = array('d', green)
            bl = array('d', blue)

            npoints = len(st)
            FI = ROOT.TColor.CreateGradientColorTable(npoints, st, re, gr, bl, ncontours)

            nLevels = 999
            levels = []
            m = max(abs(hDiff.GetMaximum()), abs(hDiff.GetMinimum()))
            zmin = -m * 1.1
            zmax = +m * 1.1
            for i in range(nLevels):
                levels.append(zmin + (zmax - zmin) / (nLevels - 1) * (i))
            levels = array('d', levels)

            hDiff.SetContour(nLevels)
            c.cd()
            # c.Divide(3,1)
            # c.cd(1)
            # hL.Draw('colz')
            # c.cd(2)
            # hR.Draw('colz')
            # c.cd(3)
            hDiff.GetXaxis().SetRangeUser(-40, 100)
            hDiff.GetZaxis().SetRangeUser(zmin, zmax)
            hDiff.Draw("colz")
            c.Update()
            style.save_canvas(c, 'SR_SL_Difference_vs_SM_run%d' % run)
        fits = {}
        for det in ['Dia', 'D0X', 'D1X', 'D2X', 'D3X', 'D0Y', 'D1Y', 'D2Y', 'D3Y']:
            # hLeftVsMaximumDia
            # cL = f.Get('c_hLeftVsMaximum%s_pfy'%det)
            # cR = f.Get('c_hRightVsMaximum%s_pfy'%det)
            cLR = f.Get('cRoot_hDeltaLeftRightVsMaximumDia%s' % det)
            cL = f.Get('cRoot_hLeftVsMaximum%s' % det)
            cR = f.Get('cRoot_hRightVsMaximum%s' % det)
            style.save_canvas(cR, 'hRightVsMaximum%s_run%d' % (det, run))
            style.save_canvas(cL, 'hLeftVsMaximum%s_run%d' % (det, run))
            style.save_canvas(cLR, 'hDeltaLeftRightVsMaximum%s_run%d' % (det, run))
            hR = copy.deepcopy(cR.GetPrimitive('hRightVsMaximum%s' % det))
            hL = copy.deepcopy(cL.GetPrimitive('hLeftVsMaximum%s' % det))
            pR = copy.deepcopy(hR.ProjectionY())
            pL = copy.deepcopy(hL.ProjectionY())
            pfR = copy.deepcopy(hL.ProfileY())  # cR.GetPrimitive('hRightVsMaximum%s_pfy'%det))
            pfY = copy.deepcopy(hR.ProfileY())  # cL.GetPrimitive('hLeftVsMaximum%s_pfy'%det))
            c.cd()
            pR.Scale(1. / pR.Integral())
            pL.Scale(1. / pL.Integral())
            i = 1
            factor = .95
            while i < pR.GetNbinsX() and pR.Integral(1, i) < factor:
                i += 1
            while i < pL.GetNbinsX() and pL.Integral(1, i) < factor:
                i += 1
            # print i,pL.GetXaxis().GetBinUpEdge(i)
            # raw_input()
            if det == 'Dia':
                ymin = -50
                ymax = 250
                xmin = 0
                xmax = 1600
            else:
                ymin = -10
                ymax = 30
                xmin = 0
                xmax = 170
            xmax = pL.GetXaxis().GetBinUpEdge(i)
            # print xmin,xmax,'\t',ymin,ymax

            ROOT.gStyle.SetOptStat(1000000001)
            ROOT.gStyle.SetOptFit(111)
            pAdd = pfR.Clone()
            pAdd.Add(pfY, -1)
            pAdd.GetXaxis().SetRangeUser(xmin, xmax)
            pAdd.GetYaxis().SetRangeUser(ymin, ymax)
            pAdd.SetStats(True)
            fit = ROOT.TF1('fit', 'pol1', 0, 1600)
            fit.SetLineColor(ROOT.kBlue)
            pAdd.Fit(fit, 'Q', '', xmin, xmax)
            fit2 = fit.Clone('fit_noOffset')
            fit2.SetLineStyle(2)
            fit2.FixParameter(0, 0)
            pAdd.Fit(fit2, 'Q+', '', xmin, xmax)
            pAdd.SetLineColor(ROOT.kBlack)
            pfY.SetLineColor(ROOT.kGreen)
            pfR.SetLineColor(ROOT.kRed)
            pAdd.SetTitle('Difference')
            fr = c.DrawFrame(xmin, ymin, xmax, ymax, ';S_{Middle} / ADC ; S / ADC')
            fr.SetStats(False)
            pAdd.Draw('same')
            pfY.Draw('same')
            pfR.Draw('same')
            pt = ROOT.TPaveText(.15, .8, .35, .9, 'NDC NB')
            pt.SetTextAlign(12)
            t1 = pt.AddText('Fit: y = m/100*x + b')
            t1.SetTextAlign(12)

            t2 = pt.AddText('m: %+5.2f #pm %5.2f' % (fit.GetParameter(1) * 100, fit.GetParError(1) * 100))
            t2.SetTextAlign(12)

            t3 = pt.AddText('b: %+5.2e #pm %5.2e' % (fit.GetParameter(0), fit.GetParError(0)))
            t3.SetTextAlign(12)
            pt.SetFillStyle(0)
            pt.SetShadowColor(0)
            pt.Draw()
            chi2_ndf = (round(fit.GetChisquare() / fit.GetNDF(), 2), fit.GetNDF())
            chi2_ndf2 = (round(fit2.GetChisquare() / fit2.GetNDF(), 2), fit2.GetNDF())
            fits[det] = [(fit.GetParameter(1) * 100, fit.GetParError(1) * 100),
                         (fit.GetParameter(0), fit.GetParError(0)),
                         (fit2.GetParameter(1) * 100, fit2.GetParError(1) * 100),
                         (fit2.GetParameter(0), fit2.GetParError(0)),
                         chi2_ndf, chi2_ndf2, xmax]
            # print fits[det]
            c.Update()
            style.save_canvas(c, 'SR_SL_Difference_vs_SM_pfy_%s_run%d' % (det, run))
        all_fits[run] = fits
    except Exception as e:
        print 'Could not convert run %d' % run, e
values = {}
for run in runs:
    if not run in all_fits.keys():
        continue
    print '%6d: ' % run
    sil = 0
    sil2 = 0
    n = 0
    s = ''
    for det in sorted(all_fits[run]):
        s += '\t' + det + '\t'
        # print '%+5.3f +/- %5.3f  |   %+5.3e +/- %5.3e'%(all_fits[run][det][1][0],
        #                                                          all_fits[run][det][1][1],
        #                                                          all_fits[run][det][2][0],
        #                                                          all_fits[run][det][2][1]),
        # print '\t|\t',
        if det != 'Dia':
            sil += abs(all_fits[run][det][2][0])
            sil2 += abs(all_fits[run][det][2][0]) ** 2
            n += 1
        s += '%+5.3f +/- %5.3f  ' % (all_fits[run][det][2][0], all_fits[run][det][2][1])
        s += '\t %s   %6.1f ' % (all_fits[run][det][5], all_fits[run][det][6])
        s += '\n'
    s += '\tSilicon average: %.3f' % (sil / n)
    try:
        s += ' +/- %.3f' % math.sqrt(sil2 / n - (sil / n) ** 2)
    except:
        s += ' %s  %s' % (sil2 / n, sil / n) + ' ERROR'
        pass
    print s
    print
