from copy import deepcopy
import ROOT
import root_style
import copy
from copy import deepcopy
from collections import OrderedDict

style  = root_style.root_style(batch=False)
style.rel_marg_top=.05
style.set_style(825,825,1,)
style.set_main_dir('output/1-intro/')

runnos = OrderedDict([(17107,'scCVD'),
                      (19106,'pCVD')])
keys = OrderedDict([
    # ('pulseheightOne','hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    # ('pulseheightTwo','hDiaTranspAnaPulseHeightOf2HighestIn10Strips'),
    # ('efficiencyTwo', 'hEfficiency_hDiaTranspAnaPulseHeightOf2HighestIn10Strips'),
    # ('efficiency','hEfficiency_hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    # ('efficiency2', 'hEfficiency_hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    ('signal','ph_strip_vs_3d_electrons')
    ])
histos = OrderedDict()
fdir = 'input/1-intro/'
stack = ROOT.THStack('stack', ';signal / #it{e}; rel. number of entries')

# ROOT.gStyle.SetFitFormat('.1f')
ROOT.gStyle.SetStatFormat('.1f')
ROOT.gStyle.SetOptStat(201)
ROOT.gStyle.SetOptFit(0)
parameters={}
for runno, typ in runnos.items():
    fname = 'ph_strip_vs_3d_electrons.{runno}.root'.format(runno=runno)
    fpath = fdir + fname
    f = ROOT.TFile.Open(fpath)
    c = f.Get('c3')
    hs = c.GetPrimitive('hs3')
    h = hs.GetHists().FindObject('hLandauStrip_trans_electrons').Clone()
    if c:
        c.UseCurrentStyle()
        c.Draw()
        raw_input()

