from copy import deepcopy
import ROOT
import root_style
import copy
from copy import deepcopy
from collections import OrderedDict

style  = root_style.root_style(batch=False)
style.set_style(825,825,1)
style.set_main_dir('output/3-radiation/')

runs = [191090,191130]
runno = 150042
keys = OrderedDict([
    # ('pulseheightOne','hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    # ('pulseheightTwo','hDiaTranspAnaPulseHeightOf2HighestIn10Strips'),
    # ('efficiencyTwo', 'hEfficiency_hDiaTranspAnaPulseHeightOf2HighestIn10Strips'),
    # ('efficiency','hEfficiency_hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    # ('efficiency2', 'hEfficiency_hDiaTranspAnaPulseHeightOfHighestIn10Strips'),
    ('resolutionEta', {'hname': 'hEtaCMNCorrectedVsResolutionEtaCorrectedIn10',
                    'fname': 'hEtaCMNCorrectedVsResolutionEtaCorrectedIn10',
                    'cname': 'cRoot_hEtaCMNCorrectedVsResolutionEtaCorrectedIn10',
                       'draw':'colz',
                       'ytit':'#eta',
                       'ztit':'number of entries',
                       'yrange':[0,1],
                       'xrange': [-19, 19],
                       'style':(825,825,1.2)}),
    #hRelChPosVsEtaCMN_In_10
    ('etaRelCh', {'hname': 'hRelChPosVsEtaCMN_In_10',
                       'fname': 'hRelChPosVsEtaCMN_In_10',
                       'cname': 'cRoot_hRelChPosVsEtaCMN_In_10',
                       'draw': 'colz',
                       'ytit': 'relative predicted hit position within the strip',
                       'xtit': '#eta',
                       'ztit': 'number of entries',
                       'xrange': [0, 1],
                       'yrange': [0, 1],
                       'style': (825, 825, 1.2)}),
    #    hRelChPos2VsResChargeWeighted_In_10('')
    ('RelChVsResolution', {'hname': 'hRelChPos2VsResChargeWeighted_In_10',
                  'fname': 'hRelChPos2VsResChargeWeighted_In_10',
                  'cname': 'cRoot_hRelChPos2VsResChargeWeighted_In_10',
                  'draw': 'colz',
                  'ytit': 'relative predicted hit position within the strip',
                  'ztit': 'number of entries',
                  'xrange': [-19, 19],
                  'yrange': [0, 1],
                  'style': (825, 825, 1.2)}),

    ('resolution',{'hname':'hDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred_FixedDoubleGausFit',
                   'fname':'cDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred_FixedDoubleGausFit',
                   'cname':'cDiaTranspAnaResidualEtaCorrectedIn10StripsMinusPred_FixedDoubleGausFit'})
    ])
fdir = 'input/3-radiation/'
fname = 'histograms.transparent.{runno}.root'.format(runno=runno)
fpath = fdir + fname
f = ROOT.TFile.Open(fpath)
factor = 100./7.6*1.02
noise = 79.2
histos = OrderedDict()
runno = 191090
for runno in runs:
    for key, item in keys.items():
        style_opt = item.get('style',(825,825,1))
        style.set_style(width = style_opt[0],height=style_opt[1],ratio=style_opt[2])
        if True:
            fname2 = '{fname}.{runno}.root'.format(runno=runno,fname=item['fname'])
            fpath2 = fdir + fname2
            f2 = ROOT.TFile.Open(fpath2)
            c2 = f2.Get(item['cname'])
            h = c2.GetPrimitive(item['hname'])
        else:
            h = f.Get(item)
        print h
        if h == None:
            print 'cannot find ',key
            for k in f.GetListOfKeys():
                if item in k.GetName():
                    print ' *',k
            continue
        h.UseCurrentStyle()
        if key is not 'resolution':
            h.Rebin2D(2,2)
        leg = None
        c = style.get_canvas('c')
        if True:
            hh = deepcopy(h)
            hh.UseCurrentStyle()
            hh.GetXaxis().SetTitle(item.get('xtit','residual / #mum'))
            hh.GetYaxis().SetTitle(item.get('ytit','number of entries'))
            hh.GetZaxis().SetTitle(item.get('ztit', ''))
            hh.Draw(item.get('draw',''))
            stats = hh.GetFunction('stats')

        histos[key] = deepcopy(hh)
        hh.Draw(item.get('draw',''))
        if leg:
            leg.Draw()
        hh.GetFunction("stats")
        if 'yrange' in item:
            yrange = item['yrange']
            hh.GetYaxis().SetRangeUser(yrange[0],yrange[1])
        if 'xrange' in item:
            xrange = item['xrange']
            hh.GetXaxis().SetRangeUser(xrange[0], xrange[1])

        c.Update()
        stats = hh.GetFunction("stats")
        print stats
        if  key == 'resolution' and stats != None:
            ROOT.gStyle.SetFitFormat('4.2g')
            hh.Draw()
            stats.SetTextSize(.03)
            print stats,stats.GetX1NDC(),stats.GetX2NDC(),stats.GetY1NDC(),stats.GetY2NDC()
            stats.SetX1NDC(0.64)
            stats.SetX2NDC(0.99)
            stats.SetY1NDC(0.61)
            stats.SetY2NDC(0.99)


        style.save_canvas(c,'{key}.{runno}'.format(key=key,runno=runno))
        # raw_input()
    if False:
        stack = ROOT.THStack('stack',';signal / #it{e}; rel. number of entries')
        leg = style.make_legend(.6,.9,2)
        for k,h in histos.items():
            if 'pulse' in k:
                h.SetLineColor(style.get_next_color())
                h.Scale(1./h.GetBinContent(h.GetMaximumBin()))
                stack.Add(h)
                if k.endswith('One'):
                    leg.AddEntry(h,'one channel','l')
                elif k.endswith('Two'):
                    leg.AddEntry(h, 'two channels', 'l')
        c.SetGridx(False)
        stack.Draw('nostack')
        stack.GetXaxis().SetRangeUser(0, 8000)
        leg.Draw()
        style.save_canvas(c,'pulseheightBoth.{runno}'.format(key=key,runno=runno))
