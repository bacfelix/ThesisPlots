
import ROOT
import root_style
import copy

style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
if True:
    f = ROOT.TFile.Open('input/3-radiation/histograms.pedestal.17000.root')
    c3 = f.Get('c_NumberOfClusters_D2Y')
    h =  copy.deepcopy( c3.GetPrimitive('NumberOfClusters_D2Y'))
    h.GetXaxis().SetTitle('number of clusters')
    h.GetYaxis().SetTitle('number of entries')
    style.set_style(825, 825, 1)
    c1 = style.get_canvas('c1')

    style.adjust_histo_style(h)
    c1.cd()
    h.Draw()
    style.save_canvas(c1,'number_of_clusters_sil')

    for p in f.GetListOfKeys():
        if p.GetName().startswith('c_hClusterSize') and p.GetName().endswith('_D2Y'):
            print p.GetName()
            c3 = f.Get(p.GetName())
            break

    h2 = copy.deepcopy(c3.GetPrimitive( c3.GetName()[2:]))
    style.set_style(825, 825, 1)
    c2 = style.get_canvas('c2')
    c2.cd()
    style.adjust_histo_style(h2)
    h2.GetXaxis().SetTitle('cluster size')
    h2.GetYaxis().SetTitle('number of entries')
    c2.cd()
    h2.Draw()
    style.save_canvas(c2,'clustersize_sil')
