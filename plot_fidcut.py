from numpy import array as ar
from collections import OrderedDict
import ROOT
import root_style
import copy
import array
from copy import deepcopy
from collections import OrderedDict
style  = root_style.root_style(batch=False)
style.set_main_dir('output/3-radiation/')
# scales =
fnames = OrderedDict([('ExactlyOneSilicon','chFidCutSilicon_OneAndOnlyOneCluster.17101.root'),
         ('DiamondCluster','chFidCutSilicon_OneAndOnlyOneCluster_DiamondCluster.17101.root')])
fdir = 'input/3-radiation/'
def save_profiles(key,histos,colors):
    print histos
    style.set_style(825, 825, 1)
    pxs = []
    pys = []
    stack_px = ROOT.THStack('s_x',';avrg. silicon x-position;rel. number of entries')
    stack_py = ROOT.THStack('s_x', ';avrg. silicon y-position;rel. number of entries')
    leg = style.make_legend(.55,.93,len(histos))

    for key,h  in histos.items():
        px = h.ProjectionX("px")
        px.SetName("x projection")
        py = h.ProjectionY("py")
        py.SetName("y projection")
        px.GetYaxis().SetTitle('number of entries')
        py.GetYaxis().SetTitle('number of entries')
        px.GetYaxis().SetTitleOffset(1.7)
        py.GetYaxis().SetTitleOffset(1.7)
        # px.Scale(1. / px.GetBinContent(px.GetMaximumBin()))
        # py.Scale(1. / py.GetBinContent(py.GetMaximumBin()))
        px.SetLineColor(colors[key])
        py.SetLineColor(colors[key])
        pxs.append(deepcopy(px))
        pys.append(deepcopy(py))
        stack_px.Add(pxs[-1])
        stack_py.Add(pys[-1])
        leg.AddEntry(pxs[-1],key,'l')
    canvas = style.get_canvas('c_xy')
    stack_px.Draw('nostack')
    stack_px.SetMaximum(1.3*stack_px.GetMaximum())
    leg.Draw()
    style.save_canvas(canvas,"selection_px")
    stack_py.Draw('nostack')
    stack_py.SetMaximum(1.3*stack_py.GetMaximum())
    leg.Draw()
    style.add_root_object(stack_px)
    style.add_root_object(stack_py)
    style.add_root_object(leg)
    style.save_canvas(canvas, "selection_py")

histos = OrderedDict()
colors = {}
i = 1
for key,fname in fnames.items():
    style.set_style(1000, 800, 1)
    canvas = style.get_canvas('c')
    fpath = fdir+fname
    f = ROOT.TFile.Open(fpath)
    c = f.Get(f.GetListOfKeys().At(0).GetName())
    fidcuts = OrderedDict()
    for p in c.GetListOfPrimitives():
        if 'hFidCutSilicon' in p.GetName():
            print 'found histo',p
            h = deepcopy(p)
        elif 'fidCut_' in p.GetName():
            print 'found fidcut',p
            fidcuts[p.GetName()] = deepcopy(p)
    canvas.cd()
    h.UseCurrentStyle()
    h.GetXaxis().SetTitle('avrg. silicon x-position / ch no.')
    h.GetYaxis().SetTitle('avrg. silicon y-position / ch no.')
    h.GetZaxis().SetTitle('number of entries')
    h.Rebin2D(2,2)
    h.Draw('colz')

    palette = h.GetListOfFunctions().FindObject("palette");
    palette.SetX1NDC(1-style.rel_marg_left)
    palette.SetX2NDC(1-style.rel_marg_left+.05)
    palette.SetY1NDC(style.rel_marg_bot)
    palette.SetY2NDC(1-style.rel_marg_top)
    canvas.Modified()
    for fc in fidcuts.values():
        fc.Draw('same')
    style.save_canvas(canvas,'selection_{key}'.format(key=key))
    histos[key] =deepcopy(h)
    colors[key] = i
    i+=1
save_profiles(key,histos,colors)

